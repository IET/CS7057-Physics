\documentclass[a4paper]{report} % V1.2
\usepackage[]{minted} 	%Code highlighting
\usepackage{gensymb}	%Degree symbol
\usepackage{listings}	%Alternate code listings
\usepackage{float}
% \usepackage{tcolorbox}
% \usepackage{etoolbox}
% \BeforeBeginEnvironment{minted}{\begin{tcolorbox}}%
% \AfterEndEnvironment{minted}{\end{tcolorbox}}%
\lstset{language=c++, aboveskip=0pt}
\usepackage{etoolbox}

%\doi{10.1145/1559755.1559763}

% Copyright
%\setcopyright{acmcopyright}

\begin{document}

% Page heads
%\markboth{Daniel Walsh \& Evan White}{CS7032 - Artificial Intelligence Project Report}

% Title portion
\title{CS7057: Real-Time Physics - Cumulative Submission 1-5}


\author{
	%\alignauthor
		Daniel Walsh\\
	 	{Trinity College Dublin}\\
		{walshd15@tcd.ie}
}

\maketitle

\section{Particle System}

In my implementation of a particle system, there is a particle system class that contains all of the particles and functions to manipulate them, as well as the definition of a particle's state, shown in Listing~\ref{listing:particle_struct}. In the demo program mass was kept constant at 1 but could be larger depending on the application.

\begin{listing}[ht]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}

struct Particle {
	float age;
	float mass;
	vec3 position;
	vec3 velocity;

	vec3 netForce;
};
\end{minted}
\caption{The struct used to hold each particle's state.}
\label{listing:particle_struct}
\end{listing}

Forces may be applied to the particle system by passing either a constant vector or a pointer to a function that either takes a position or an entire particle as input, as shown in Listings~\ref{listing:apply_force_constant}~and~\ref{listing:apply_force_field}. This allows a lot of flexibility around the kind of forces that may be applied to the particle system.

\begin{listing}[ht]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void ParticleSystem::ApplyForce(vec3 f) 
{
 	for (int i =0; i < numParticles; i++)
  	{
    	particles[i].netForce += f;
  	}
}
\end{minted}
\caption{Application of a constant force to all particles.}
\label{listing:apply_force_constant}
\end{listing}

\begin{listing}[ht]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void ParticleSystem::ApplyForce(vec3 (*f) (vec3))
{
	for (int i =0; i < numParticles; i++)
  	{
    	(particles[i].netForce) += (*f)(particles[i].pos);
  	}
}
\end{minted}
\caption{Application of a force based on position to all particles in the system. It is also possible for functions to take the whole particle as a parameter to access other attributes.}
\label{listing:apply_force_field}
\end{listing}

Listing~\ref{listing:euler_step} shows the euler step function that is called each frame to update the particles in the particle system. If particles have exceeded their lifetime, they are simply returned to the 
base position of the particle system and if an emission function is defined, a force will be applied to the particles of age 0 according to the defined function. An issue with this approach is that all of the particles are always drawn so the system is likely to be bandwidth limited for a large number of particles.

\begin{listing}[ht]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void ParticleSystem::EulerStep(float deltaTime)
{
  for (int i =0; i < numParticles; i++){
    Particle* p = &particles[i];
    
    
    for (int i =0; i < potentialColliders.size(); i++)
    {
      TestCollision(p, potentialColliders[i]);  
    }

    (p->velocity) = ((p->netForce) / p->mass) * deltaTime;
    (p->position) = (p->position) + ((p->velocity) * deltaTime);
    p->age += deltaTime;

    if(p->age > particleLifeTime){
      p->position = vec3(0.0f, 0.0f, 0.0f);
      p->netForce = vec3(0.0f, 0.0f, 0.0f);
      p->velocity = vec3(0.0f, 0.0f, 0.0f);
      p->age = 0;
    }

    if (this->emissionFunction != nullptr)
      Emit(deltaTime);
  }
}
\end{minted}
\caption{The Euler Step Function called once per frame to update the state of the particles in the system.}
\label{listing:euler_step}
\end{listing}

\pagebreak

\section{Unconstrained Rigid-body Motion}

Listing~\ref{listing:rigid-body_state} shows the state variables used to define a rigid-body. The same functions that were used to apply forces to a particle system can be used again here but in this case, torque is calculated as a cross product of the force with a vector from the object's center of mass to the point of application of the force.

\begin{listing}[ht]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
class PhysObject : public Object{
	vec3 linearMomentum;
	vec3 angularMomentum;
	
	vec3 centreOfMass;

	vec3 netForce;
	vec3 netTorque;

	float inverseMass;
	void Update(float timestep);
}
\end{minted}
\caption{Excerpt from the PhysObject class showing how state is defined.}
\label{listing:rigid-body_state}
\end{listing}

Listing~\ref{listing:rigid-body_update} shows the update function for a rigid-body that is called each simulation step. Orientation is not explicitly stored since it can be obtained from the model matrix and re-normalised and re-orthogonalised once updated.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}

void PhysObject::Update(float timeStep) 
{
	//Orientation matrix is equal to the upper left 3x3 section of model matrix
	mat3 orientation	= GetOrientationMatrix();
	linearMomentum	+= (netForce);
	position += (linearMomentum * inverseMass) * timeStep;

	mat3 inertia = orientation * inverseInertiaTensor * transpose(orientation);
	angularMomentum += inertia * netTorque;
	vec3 angularVelocity = (angularMomentum * inverseMass);

	mat3 rotationUpdate = mat3( 0.0f, -angularVelocity.z, angularVelocity.y,
								angularVelocity.z,	0.0f, -angularVelocity.x,
								-angularVelocity.y, angularVelocity.x,	0.0f );

	//Compute Rotational Update
	mat3 rotationMatrix = orientation + 
		(timeStep * (rotationUpdate * orientation));

	//Normalise and Re-Orthogonalise Axes
	rotationMatrix[0] = normalize(rotationMatrix[0]);
	rotationMatrix[1] = cross(rotationMatrix[2], rotationMatrix[0]);
	rotationMatrix[1] = normalize(rotationMatrix[1]);
	rotationMatrix[2] = cross(rotationMatrix[0], rotationMatrix[1]);
	rotationMatrix[2] = normalize(rotationMatrix[2]);
	
	//Apply Rotation
	modelMatrix[0].x = rotationMatrix[0].x;
	modelMatrix[0].y = rotationMatrix[0].y;
	modelMatrix[0].z = rotationMatrix[0].z;

	modelMatrix[1].x = rotationMatrix[1].x;
	modelMatrix[1].y = rotationMatrix[1].y;
	modelMatrix[1].z = rotationMatrix[1].z;

	modelMatrix[2].x = rotationMatrix[2].x;
	modelMatrix[2].y = rotationMatrix[2].y;
	modelMatrix[2].z = rotationMatrix[2].z;

	//Apply Translation
	modelMatrix[3] = vec4(position, modelMatrix[3].w);

	//Apply Scale
	modelMatrix = scale(modelMatrix, scaleFactor);

	//Reset Accumulators
	this->netForce = vec3(0);
	this->netTorque = vec3(0);
	
	//Update BVH
	bvh.Update(&modelMatrix);
}
\end{minted}
\caption{Rigid-body Update Method Called Each Simulation Step.}
\label{listing:rigid-body_update}
\end{listing}

After updating the state of the rigid-body, the scale of the object is restored and the BVH is updated based on the object's model matrix.

\newpage

\section{Broad Phase Collision Detection}

Broad phase collision detection is implemented as simply a brute-force test between all of the objects' bounding spheres. Spatial subdivision was implemented (in the SpatialGrid class) at this stage but it was later removed because the update cost was too large. Also, the static class IntersectionTest contains OBB/OBB, sphere/triangle, and triangle/triangle intersection tests but for ease of implementation only sphere/sphere tests are used for collision detection and sphere/triangle tests are used later to cull empty spheres in Octree construction.

Bounding volumes are constructed based on the size of the object and updated each frame by passing a pointer to the object's model matrix. This is then used to update the position and scale of the bounding volume. The update function for a bounding sphere is shown in Listing~\ref{listing:bs_update} below. This function currently assumes only uniform scaling but to support non-uniform scaling, it would just have to test each axis and find the maximum length.


\begin{listing}[h!]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void BoundingSphere::Update(mat4 *modelMat)
{
	this->modelMatrix = modelMat;

	position = (*modelMatrix) * vec4(center, 1.0f);

	this->radius = localRadius * length((*modelMatrix)[0]);
}
\end{minted}
\caption{Bounding Sphere Update Method, Called Each Simulation Step.}
\label{listing:bs_update}
\end{listing}
 
 
 
\pagebreak

\section{Narrow Phase Collision Detection}

For narrow phase collision detection, an Octree is constructed to subdivide the object space into increasingly granular levels. If two objects are colliding at one level then  the next level down is tested, otherwise they are deemed to not collide. The octree construction algorithm removes any nodes that do not contain any triangles, thus reducing the size of the search space but the structure is probably not strictly speaking an octree anymore.

If multiple collision points are found between two objects, the average of all of them is taken. This is just a simple approximation that allows for the modeling of different types of contact without fundamentally changing the algorithm. Listing~\ref{listing:narrow_phase} shows the complete narrow phase method and Listing~\ref{listing:narrow_phase_collsion} shows how the collision detection step is performed.

For the case of inter-penetration, objects are simply translated away from each other along the collision normal by the penetration depth. This is, again, only a rough approximation and translating back along the object's momentum vectors should provide better results but it was found in practice that this was less effective at completely mitigating inter-penetration.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
bool NarrowPhase(PhysObject *obj1, PhysObject *obj2, 
vector<BVHNode> &collidingNodes)
{
	std::vector<BVHNode> collidingNodes;
	TestCollision(obj1->bvh.rootNode, obj2->bvh.rootNode, collidingNodes);
	
	if (collidingNodes.size() == 0)
	{
		return false;
	}
	else
	{
		vec3 point1, point2;
		float penetrationDepth = (collidingNodes[1].boundingSphere.radius
		 + collidingNodes[0].boundingSphere.radius)
		- length(collidingNodes[1].boundingSphere.position 
			- collidingNodes[0].boundingSphere.position); 
	
		int i;
		for( i =0; i < collidingNodes.size(); i++)
		{
			if (i%2 == 0)
			{
				point1 += vec3(collidingNodes[i].boundingSphere.position);
			}
			else
				point2 += vec3(collidingNodes[i].boundingSphere.position);

			if (i < collidingNodes.size() - 1)
			{
				float dist = (collidingNodes[i+1].boundingSphere.radius 
						+ collidingNodes[i].boundingSphere.radius)
						- length(collidingNodes[i + 1].boundingSphere.position 
						- collidingNodes[i].boundingSphere.position);

				if (dist > penetrationDepth)
					penetrationDepth = dist;
			}
		}

		point1 /= (float)(i/2);	
		point2 /= (float)(i/2);

		if (penetrationDepth > 0)
		{
			vec3 n = normalize(point2 - point1);

			obj1->position -= (n * (penetrationDepth));
			obj2->position += (n * (penetrationDepth));
		}
	
		CollisionResponse(obj1, obj2, point1, point2);
	}

	return true;
}
\end{minted}

\caption{Narrow Phase Method.}
\label{listing:narrow_phase}
\end{listing}

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void TestCollision(BVHNode *node1, BVHNode *node2, 
vector<BVHNode> &collidingNodes)
{
	if (!IntersectionTest::TestIntersection(&node1->boundingSphere,
	 &node2->boundingSphere))
		return;
	else
	{
		if (node1->children.size() > 0 && node2->children.size() > 0)
		{
			for (int i = 0; i < node1->children.size(); i++)
			{
				for (int j = 0; j < node2->children.size(); j++)
				{
					if (IntersectionTest::TestIntersection(
					&node1->children[i]->boundingSphere, 
					&node2->children[j]->boundingSphere))
					{
						TestCollision(node1->children[i], 
						node2->children[j], collidingNodes);
					}
				}
			}
			return;
		}
		else
		{
			return;
		}
	}

	collidingNodes.push_back(*node1);
	collidingNodes.push_back(*node2);
}
\end{minted}
\caption{Narrow Phase Collision Detection Method.}
\label{listing:narrow_phase_collsion}
\end{listing}

\pagebreak

\section{Collision Response}

Once the contact points and collision normal have been found in the narrow phase, collision response is applied as a simple impulse force on each object, whose magnitude is calculated by Equation~\ref{eq:impulse_force}.

\begin{equation}
\label{eq:impulse_force}
j = \frac{-\left( 1+\epsilon \right)\cdot \vec{V_{rel}} }{\frac{1}{M_a} + \frac{1}{M_B} + \hat{n} \cdot \left( I_{a}^{-1} \left( r_a \times \hat{n} \right)\right) \times r_a +  \hat{n} \cdot \left( I_{b}^{-1} \left( r_b \times \hat{n} \right)\right)}
\end{equation}

The code used to calculate and apply this impulse is shown in Listing~\ref{listing:collision_response}

\begin{listing}[h]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
bool CollisionResponse(PhysObject *obj1, PhysObject *obj2, vec3 &c1, vec3 &c2)
{
	float epsilon = obj1->GetRestitutionCoeffcient() * 
				obj2->GetRestitutionCoeffcient();
	
	vec3 c = point1 + ((point2 - point1)/2.0f);

	vec3 r1 = c1 - obj1->GetCentreOfMass();	
	vec3 r2 = c2 - obj2->GetCentreOfMass();

	vec3 contactNormal = normalize(c2 - c1);

	vec3 p1 = obj1->GetVelocity() + cross(obj1->GetAngularVelocity(),
			 c - obj1->GetCentreOfMass());
	vec3 p2 = obj2->GetVelocity() + cross(obj2->GetAngularVelocity(), 
			c - obj2->GetCentreOfMass());

	float relativeVelocity = dot(contactNormal, (p1 - p2));


	if (relativeVelocity > -COLLISION_THRESHOLD 
		&& relativeVelocity < COLLISION_THRESHOLD)	//Resting contact?
	{
		obj1->Stop();
		obj2->Stop();
		return false;
	}

	if (relativeVelocity < -COLLISION_THRESHOLD)		//Moving Away
	{
		return false;
	}

	mat3 I1 =  obj1->GetOrientationMatrix() * 
			obj1->GetInverseInertiaTensor() * 
				transpose(obj1->GetOrientationMatrix()) ;

	mat3 I2 =	obj2->GetOrientationMatrix() *
			 obj2->GetInverseInertiaTensor() * 
				transpose(obj2->GetOrientationMatrix()) ;

	float N = -1.0f * (1 + epsilon) * relativeVelocity;

	float t1t2 = obj1->inverseMass + obj2->inverseMass;
	vec3 t3 = cross(I1 * cross(r1, contactNormal), r1);
	vec3 t4 = cross(I2 * cross(r2, contactNormal), r2);

	float t3t4 = dot(t3, contactNormal) + dot(t4, contactNormal);

	float denominator = t1t2 + t3t4;

	float impulse = N/denominator;

	DebugOutput::DrawLine(c1, c1 + (contactNormal * impulse));

	obj1->ApplyForce(impulse * contactNormal, c1);
	obj2->ApplyForce(impulse * -contactNormal, c2);

	return true;
}
\end{minted}
\caption{Impulse Response Calculation and Application.}
\label{listing:collision_response}
\end{listing}

% Bibliography
%\bibliographystyle{abbrv}
%\bibliography{references}
\end{document}
