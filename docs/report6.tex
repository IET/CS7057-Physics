\documentclass[a4paper]{report} % V1.2
\usepackage[]{minted} 	%Code highlighting
\usepackage{gensymb}	%Degree symbol
\usepackage{listings}	%Alternate code listings
\usepackage{float}
\usepackage{caption}
\usepackage{graphicx}	%For includegraphics in figures
\usepackage{hyperref}

% \usepackage{tcolorbox}
% \usepackage{etoolbox}
% \BeforeBeginEnvironment{minted}{\begin{tcolorbox}}%
% \AfterEndEnvironment{minted}{\end{tcolorbox}}%
\lstset{language=c++, aboveskip=0pt}
\usepackage{etoolbox}

%\doi{10.1145/1559755.1559763}

% Copyright
%\setcopyright{acmcopyright}

\begin{document}

% Title portion
\title{CS7057: Real-Time Physics - Assignment 6}


\author{
	%\alignauthor
		Daniel Walsh\\
	 	{Trinity College Dublin}\\
		{walshd15@tcd.ie}
}

\maketitle

\section{Introduction}

\begin{figure}[ht]
\centering
\includegraphics[width=13cm]{deforming_bar.png}
\caption{Screenshot of Deformed Bar From Running Application}
\label{fig:deforming_bar}
\end{figure}

For this assignment I have implemented demo of a bar deforming under its own weight based on the paper ``Stable Real-time Deformations'' by M\"{u}ller et al \cite{Muller2002}. The programs starts with the bar undisturbed and the `G' key may be used to switch gravity on or off to observe the bar deforming and restoring itself to its original state.

\section{Implementation Details}
This implementation largely follows the method from chapter ten of the book ``Physics Based Animation'' by Kenny Erleben et al.\cite{PBA}.

\subsection{Tetrahedral Mesh Geometry}
The finite element method requires a volumetric representation of geometry and so Tetgen\cite{tetgen} was used to convert a surface mesh in .ply format to a tetrahedral mesh in .mesh format. A custom importer was implemented to load the volumetric geometry from the mesh file and construct the data structures necessary for physics and rendering.

\subsection{Stiffness Warping}
Stiffness warping is a method  to allow the use of linear stress approximations in the finite element method without the over-stretching under large deformations that these approximations would typically cause. This is achieved by storing a stiffness matrix for each tetrahedral element at initialisation time and using the current orientation to warp the stiffness matrices for each node when calculating displacements for the deformed state. This greatly reduces the computational complexity of each timestep since the updated orientation may be easily calculated from the edge vectors of the element.

\subsection{Update Loop}
The update loop as used in ``Physics Based Animation'' is shown in listing~\ref{listing:update_loop}.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
Update(float dt)
{
	//Update element orientation matrices
	UpdateOrientation();

	//Construct Warped Stiffness Matrices
	StiffnessAssembly();	

	//If modeling plasticity, add appropriate forces
	AddPlasticityForce(dt);

	//find A matrices and b vectors
	DynamicsAssembly(dt);

	//Use a Conjugate Gradient Solver to Solve Av = b
	ConjugateGradient(A, b);

	//Update node positions based on velocities computed in the previous step
	PositionUpdate(dt);
}
\end{minted}
\caption{Warped Stiffness Update Loop}
\label{listing:update_loop}
\end{listing} 

To calculate the warped stiffness, two equations must be solved $K' = RKR^{-1}$ for the stiffness matrices and $f_u' = -RKx_u$ for the force offset vectors. A sample implementation is shown in Listing~\ref{listing:stiffness_assembly}.


\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void TetrahedralMesh::CalculateWarpedStiffness(const FEMElement &e)
{
	//K' = R * K * R^-1
	glm::mat3 ReT = glm::transpose(e.localRotation);
	
	for (int i =0; i < 4; i++)
	{
		glm::vec3 f = glm::vec3(0);

		for (int j =0; j < 4; j++)
		{
			f += e.Ke[i][j] *  verts[e.indices[j]].X0;

			if (j >= i)
			{
				glm::mat3 tmp = e.localRotation * e.Ke[i][j] * ReT;

				// f_u = -R * K * X0
				verts[e.indices[i]].K_Warp[e.indices[j]] += tmp;

				if (verts[e.indices[i]].K_Warp[e.indices[j]] == glm::mat3(0))
					assert(0);

				if(j > i)
				{
					verts[e.indices[j]].K_Warp[e.indices[i]] += glm::transpose(tmp);
				}
			}

		}
		verts[e.indices[i]].F_u -= e.localRotation * f;
	}
}
\end{minted}
\caption{Calculation of Warped Stiffness Matrices}
\label{listing:stiffness_assembly}
\end{listing} 

The dynamics assembly stage is concerned with calculating the values that will be used in the conjugate gradient solver. This means solving $A = \left( M + \Delta t C + \Delta t^2 K\right)$ (where M is the mass matrix, and C is the damping matrix), and $b = Mv^i - \Delta t \left( K x^i + f_u - f_{ext} \right)$ to construct the equation $Av^{i+1} = b$. A sample implementation of this stage is given in Listing~\ref{listing:dynamics_assembly}.


\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
void TetrahedralMesh::DynamicsAssembly(float deltaTime)
{
	float dt2 = deltaTime * deltaTime;

	for (int i = 0; i < verts.size(); i++)
	{
		verts[i].b = glm::vec3(0);
		float m_i = verts[i].m;

		for (auto &iterator : verts[i].K_Warp)
		{
			int j = iterator.first;
			glm::mat3 Kij = iterator.second;
			
			verts[i].A[j] = Kij * dt2;

			float massDamping = m_i + (massDampingFactor * m_i * deltaTime);

			glm::vec3 Xi = Kij * verts[j].X;

			verts[i].b -= Xi;
			 
			if (i == j)
			{
				verts[i].A[j][0][0] += massDamping;
				verts[i].A[j][1][1] += massDamping;
				verts[i].A[j][2][2] += massDamping;

				if (stiffnessDampingFactor > 0)
				{
					glm::mat3 stiffnessDamping = Kij * stiffnessDampingFactor * dt2;
					verts[i].A[j] += stiffnessDamping;
				}
			}
		}
		
		verts[i].b *= deltaTime;
		verts[i].b += verts[i].velocity * m_i;
		verts[i].b -= deltaTime * (verts[i].F_u - verts[i].Fext);
	}
}
\end{minted}
\caption{Transformations Between Local and Global Space Used in the Program}
\label{listing:dynamics_assembly}
\end{listing} 


\section{Performance}
The release build of this program achieves a stable framerate of 60 FPS on a standard workstation but while debugging the framerate is close to zero.

A major contributor to this sub-optimal performance is likely that the Vertex structure from the TetrahedralMesh class is used for both physics and rendering so it is quite a large object and most of it is not necessary to load into OpenGL's vertex buffer.

Possibly related to the aforementioned framerate issues is the problem of this program not being stable under all circumstances. It is quite easy to cause the mesh vertices to dissipate completely by tweaking the material parameters of the mesh. This does seem to happen more often when the framerate drops so it is likely partially due to the increased length of the physics time step but more work would be required to determine the exact source of the instabilities. The target timestep of the simulation is 12.5ms.

\section{Links}

\href{https://youtu.be/GnzD78RsBEk}{Video}\\
\href{https://www.dropbox.com/sh/zro6gbujqjuj1d5/AAByXAio8AtNdT4hwYNjhPk7a?dl=0}{Binary}\\
\href{https://gitlab.com/IET/CS7057-Physics}{Source Code} (FEM branch)\\

% Bibliography
\bibliographystyle{abbrv}
\bibliography{references}
\end{document}
