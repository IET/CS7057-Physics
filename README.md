# Continuous Assessment For CS7057-Physics Module

## Assignment 1: Particle system with over 1,000 particles and at least two forces.
A simple implementation of a CPU based particle system.

## Assignment 2: Unconstrained rigid body motion.
Rigid body position, velocity, and orientation update based on external forces applied.

## Assignment 3: Broad-phase collision detection
Using simple bounding spheres to identify potentially colliding objects.

## Assignment 4: Narrow-phase collision detection and contact modelling
Implementation of narrow-phase collision detection using a bounding volume hierarchy for more precise collision detection, allowing the filtering of objects that are not actually colliding and determining more precisely where collisions are occuring.

## Assignment 5: Collision Response 
This assignment concludes the rigid-body section of the course by adding collision response. It is implemented as a simple impulse response with an added correction step to ensure that no inter-penetration occurs.

## Assignment 6: Beyond Rigid Body Physics
In this assignment I chose to implement the paper [Stable Real-Time Deformations](https://graphics.ethz.ch/Downloads/Publications/Papers/2002/p_Mue02.pdf) by Müller et al.

I have constructed a simple demo with a bar fixed on one side deforming under its own weight similar to that used in the paper.

Built solution available [here](https://www.dropbox.com/sh/turhgabgvpbr8gl/AABCyWUQoj_GNAb0qhUD1U6ya?dl=0)

Video of assignemnts 1-5 [here](https://www.youtube.com/watch?v=mQoyVeWXgKw)

Video of assignment 6 [here](https://www.youtube.com/watch?v=GnzD78RsBEk)
