#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoordIn;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

out vec2 TexCoord;

void main()
{
	gl_Position = projectionMat * viewMat * modelMat * vec4(position.xyz, 1.0);
	// We swap the y-axis by substracting our coordinates from 1. This is done because most images have the top y-axis inversed with OpenGL's top y-axis.
	//TexCoord = texCoordIn;
	//TexCoord = vec2(texCoordIn.x, 1.0 - texCoordIn.y);
}