#version 330 core
in vec2 TexCoord;

out vec4 color;

// Texture samplers
uniform sampler2D tex;

void main()
{
	color = texture(tex, TexCoord);
}