#version 330 core
in vec2 TexCoord;

out vec4 color;

// Texture samplers
uniform vec3 col;

void main()
{
	color = vec4(col, 1);
}