#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 Normal;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;

uniform sampler2D texture_diffuse1;

uniform sampler2D normalMap;
uniform sampler2D specularMap;

uniform bool normalMapping;

void main()
{           
    //TODO - Ideally the light should have diffuse, specular, and ambient values

    vec3 normal = fs_in.Normal;
    mat3 tbn;
    // Obtain normal from normal map in range [0,1]
    normal = texture(normalMap, fs_in.TexCoords).rgb;
    // Transform normal vector to range [-1,1]
    normal = normalize((normal * 2.0) - 1.0);  //No need to tranform into world space, lighting calculations are happening in tangent space 
   
    // Get diffuse color
    vec3 color = texture(texture_diffuse1, fs_in.TexCoords).rgb;
    // Ambient
    vec3 ambient = 0.1 * color;
    // Diffuse
    vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;
    // Specular
    vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
	vec3 specular = spec * vec3(texture(specularMap, fs_in.TexCoords));
    
    FragColor = vec4(color, 1.0);//vec4(ambient + diffuse + specular, 1.0f);
    //FragColor = vec4(ambient + diffuse, 1.0f);
    //FragColor = vec4(1.0, 0.1, 0.6, 1.0);
    //FragColor = vec4(diffuse, 1.0f);
    //FragColor = vec4(color, 1.0);
    //FragColor = vec4(specular, 1.0);
}