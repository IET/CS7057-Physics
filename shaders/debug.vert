#version 330 core
layout (location = 0) in vec3 position;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

uniform vec4 col;

out vec4 in_color;

void main()
{
	in_color = col; 
	mat4 MVP = projectionMat * viewMat * modelMat;
	gl_Position = MVP * vec4(position.x, position.y, position.z, 1.0);	
}