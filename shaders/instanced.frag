#version 330 core

in vec2 TexCoords;
out vec4 color;

uniform sampler2D tex;

void main()
{
	vec4 sample = texture(tex, gl_PointCoord);
	//color = sample;
	//color = vec4(texture(tex, TexCoords).r, texture(tex, TexCoords).g, texture(tex, TexCoords).b, 0.0);
	//color = vec4(TexCoords.x, TexCoords.y, 0.13, sample.a);
	//color = vec4(0.1, 0.1, 0.1, texture(tex, TexCoords).r + 0.2);
	
	//Actual desired value
	color = vec4(0.88, 0.34, 0.13, 0);
	//color = sample;
}