#include "mesh.h"


class InstancedMesh : public Mesh {
public:
	InstancedMesh::InstancedMesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> texture, int instances, glm::vec3 *positions);
	InstancedMesh::InstancedMesh(const InstancedMesh &other);

	void setupMesh();

	void Draw();
	void Draw(glm::mat4 &v, glm::mat4 &p);
	
	//TODO - Make this less messy
	glm::vec3 *positions;

	~InstancedMesh();

private:
	int instances;
	GLuint positionBuffer;

};