#include "ResponseDemo.h"
#include "Physics.h"
#include "glfw3.h"
#include "ParticleForces.h"

ResponseDemo::ResponseDemo(void)
{
}

void ResponseDemo::Init()
{
	texShader = ShaderManager::loadShader("texture");
	rectangle = Model("../models/rectangle.obj");
	rectangle.setShader(texShader);

	planeModel.load("../models/plane/F-18.obj");
	planeModel.setShader(texShader);

	PhysObject obj = PhysObject(planeModel, glm::vec3(0, 0.0f, 0.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	obj.setTexture(TextureUtils::LoadTextureFromFile("../textures/plane/SopCamel(C).jpg"));
	Physics::PhysicsObjects.push_back(obj);

	obj = PhysObject(rectangle, glm::vec3(-700.0f, 700.0f, 100.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	obj.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(obj);

	obj = PhysObject(rectangle, glm::vec3(-600.0f, -200.0f, 300.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	obj.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(obj);

	obj = PhysObject(rectangle, glm::vec3(0.0f, -400.0f, 100.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	obj.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(obj);

	obj = PhysObject(rectangle, glm::vec3(500.0f, 0.0f, 300.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	obj.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(obj);

	obj = PhysObject(rectangle, glm::vec3(-600.0f, 100.0f, 500.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	obj.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(obj);
}


void ResponseDemo::Update(float dt)
{
	collidingNodes.clear();

	//Test collisions
	for (int i =0; i < Physics::PhysicsObjects.size() - 1; i++)
	{
		for (int j = i + 1; j < Physics::PhysicsObjects.size(); j++)
		{
			//collisionTests += 1.0f;
			if (IntersectionTest::TestIntersection(&Physics::PhysicsObjects[i].bs, &Physics::PhysicsObjects[j].bs))
			{
				Physics::NarrowPhase(&Physics::PhysicsObjects[i], &Physics::PhysicsObjects[j], collidingNodes, dt);
			}
		}
	}

	//Apply attrction force to physics objects
	for (int i = 0; i < Physics::PhysicsObjects.size(); i++)
		Physics::PhysicsObjects[i].ApplyForce(&ParticleForces::Attraction, glm::vec3(0.0f, 20.0f, 0.0f));
}

void ResponseDemo::Draw(glm::mat4 v, glm::mat4 p)
{
	for (int i = 0; i < Physics::PhysicsObjects.size(); i++)
		Physics::PhysicsObjects[i].Draw(v, p);

	for (int i = 0; i < collidingNodes.size(); ++i)
		collidingNodes[i].boundingSphere.Draw(v, p, glm::vec3(1.0f, 0.0f, 0.0f));		

}

void ResponseDemo::HandleInput(bool *keys)
{
	if(keys[GLFW_KEY_UP])
	{	
		if(keys[GLFW_KEY_LEFT_SHIFT])
		{
			Physics::PhysicsObjects.front().position -= glm::vec3(0.0f, 0.0f, 10.0f);
		}
		else
			Physics::PhysicsObjects.front().position += glm::vec3(0.0f, 10.0f, 0.0f);
	}
	if(keys[GLFW_KEY_DOWN])
	{
		if(keys[GLFW_KEY_LEFT_SHIFT])
		{
			Physics::PhysicsObjects.front().position += glm::vec3(0.0f, 0.0f, 10.0f);
		}
		else
			Physics::PhysicsObjects.front().position -= glm::vec3(0.0f, 10.0f, 0.0f);
	}

	if(keys[GLFW_KEY_LEFT])
	{
		Physics::PhysicsObjects.front().position -= glm::vec3(10.0f, 0.0f, 0.0f);
	}
	if(keys[GLFW_KEY_RIGHT])
	{
		Physics::PhysicsObjects.front().position += glm::vec3(10.0f, 0.0f, 0.0f);
	}
}

void ResponseDemo::Cleanup()
{
}

ResponseDemo::~ResponseDemo(void)
{
}
