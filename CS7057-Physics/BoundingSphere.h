#pragma once
#include <glm/glm.hpp>
#include "model.h"
#include "Shader.hpp"

class BoundingSphere
{
public:
	BoundingSphere(void);
	~BoundingSphere(void);

	BoundingSphere(Model* m);
	BoundingSphere(std::vector<Mesh::Vertex*> *verts);
	BoundingSphere(std::vector<Triangle*> &triangles);
	BoundingSphere(glm::vec3 pos, float rad);
	BoundingSphere(glm::vec3 min, glm::vec3 max);

	void GenerateBuffers();

	void Update(glm::mat4 *modelMat);
	void Draw(glm::mat4 v, glm::mat4 p, glm::vec3 color = glm::vec3(0.0f, 1.0f, 0.0f));


	void ComputeRadius(Model* m);
	void ComputeRadius(std::vector<Mesh::Vertex*> *verts);
	void ComputeRadius(std::vector<Triangle*> &triangles);

	
	glm::vec4 position;
	glm::vec3 obbMaxBounds;
	glm::vec3 obbMinBounds;
	float radius;

	glm::mat4 *modelMatrix;

	//For Debug Rendering
	static GLuint VBO, VAO;
	static Shader *shader;

	bool isColliding;

public:
	float localRadius;
	//Model-space center of the object
	glm::vec3 center;
};

