#pragma once
#include <vector>
#include "mesh.h"
#include "BoundingSphere.h"
#include "SphereTree.h"

class Octree : public SphereTree
{
public:
	Octree(void);
	Octree(std::vector<Mesh::Vertex> &verts,int inDepth);
	Octree(std::vector<Triangle> &triangles, int inDepth);

	void Init(std::vector<Triangle> &triangles, int inDepth);

	void GenerateSpheres(BVHNode*& node, int depth);
	
	void Update(glm::mat4 *modelMatrix);
	void UpdateNode(BVHNode *node, glm::mat4 *modelMatrix);

	~Octree(void);

private:
//	std::vector<Vertex>* vertices;
};

