//Courtesy of http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>

#include <stdlib.h> //rand
#include <time.h> 

#include "Shader.hpp"

struct Texture {
	GLuint id;
	string type;
	aiString path;
};

class Mesh {
public:
	struct Vertex {
		// Position
		glm::vec3 Position;
		// Normal
		glm::vec3 Normal;
		// TexCoords
		glm::vec2 TexCoords;
		// Tangent
		glm::vec3 Tangent;
		// Bitangent
		glm::vec3 Bitangent;
	};
	/*  Mesh Data  */
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;
	GLuint VAO;

	/*  Functions  */
	// Constructors
	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);

	// Render the mesh
	void Draw(Shader* shader);


	~Mesh();

private:
	bool		isInstanced;
	int			instances;

	/*  Functions    */
	// Initializes all the buffer objects/arrays
	void setupMesh();

protected:
	Mesh();	//TODO - Implement some kind of stub

	/*  Render data  */
	GLuint VBO, EBO;

};



