//Expanded from the code at http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
//using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessary OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <IL/il.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "mesh.h"
#include "Shader.hpp"
#include "ShaderManager.hpp"

GLint TextureFromFile(const char* path, std::string directory, bool gamma = false);

struct Triangle
{
	glm::vec3 points[3];
	glm::vec3 centroid;
	//glm::vec3 planeNormal;

	Triangle(glm::vec3 &p0, glm::vec3 &p1, glm::vec3 &p2)
	{
		points[0] = p0;
		points[1] = p1;
		points[2] = p2;

		centroid = (p0 + p1 + p2)/3.0f;
		
		//planeNormal = glm::normalize( glm::cross( *points[1] - *points[0], *points[2] - *points[0]));
	}

	Triangle(Mesh::Vertex *v0, Mesh::Vertex *v1, Mesh::Vertex *v2)
	{
		points[0] = v0->Position;
		points[1] = v1->Position;
		points[2] = v2->Position;

		centroid = (points[0] + points[1] + points[2])/3.0f;
		
		//planeNormal = glm::normalize((v0->Normal + v1->Normal + v2->Normal)/3.0f);
	}
};


class Model
{
	friend class Mesh;

public:
	/*  Model Data */
	std::vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
	std::vector<Mesh> meshes;
	
	std::map<glm::vec3, Triangle*> triangleMap;
	std::vector<Triangle> triangles;

	std::string directory;
	bool gammaCorrection;

	//TODO - Move these to physObject class
	glm::vec3 minBounds;
	glm::vec3 maxBounds;

	//TODO - Move these to Object class
	glm::vec3 scaleFactor;

	//Default constructor
	Model();

	// Constructor, expects a filepath to a 3D model.
	Model(std::string const & path, bool gamma = false);

	// Draws the model, and thus all its meshes
	void Draw(glm::mat4 model);

	glm::mat4 getModelMatrix();

	Shader* getShader();

	void setShader(Shader* s);

	void load(string path);

	~Model();

private:
	glm::vec3 location;
	glm::vec3 rotation;

	Shader* shader;

	/*  Functions   */
	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene);

	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	
	GLint TextureFromFile(const char* path, string directory, bool gamma = false);

	void GenerateTriangleMap();
protected:
	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);

	// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes std::vector.
	void loadModel(string path);
};
