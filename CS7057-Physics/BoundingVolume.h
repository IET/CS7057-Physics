#pragma once
#include <glm/glm.hpp>
#include <GL/glew.h>
#include "Shader.hpp"
#include "ShaderManager.hpp"

//TODO - this should probably be a K-dop class w/ OBB being a special case of K-Dop where order is 2 (numVerts = 2^2)
//Then Bounding Sphere would have to be a seperate entity.
//Maybe collision tests should actually be done in the Physics.h file?
//Like TestCollision(K-Dop, K-Dop)
//TestCollision(BoundingSphere, K-Dop)
//TestCollision(BoundingCylinder, K-Dop)
//TestCollision(BoundingCylinder, BoundingSphere)
//Although the issue there is that order matters so you'd actually need a lot more (although maybe (K-Dop, sphere) could just call (sphere, K-dop) w/ its parameters)

//summary of intersection tests here:
//http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?page=4

class BoundingVolume
{
public:
	BoundingVolume(void);
	~BoundingVolume(void);
	
	//Update is not needed for spheres but whatever, I need it for OBBs anyway
	void Update(glm::mat4 *modelMat);
	
	virtual void Draw(glm::mat4 v, glm::mat4 p) = 0;
	
	//Backwards compatibility w/ Particle system
	//TODO - Delete
	glm::vec4 GetWorldMaxBounds();
	glm::vec4 GetWorldMinBounds();

	glm::vec4 GetMaxBounds();
	glm::vec4 GetMinBounds();

	glm::vec4 GetPosition();

	glm::vec4 position;

	//Object already has orientation in terms of modelMatrix, this is just data duplication
	//Orientation matrix to store planes for point-plane collision test   
	//glm::mat3 orientation;
protected:
	bool isColliding;
	//For Debug Rendering
	GLuint VBO, VAO, EBO;

	Shader* shader;


	glm::vec4 maxBounds;
	glm::vec4 minBounds;

	//Deal with changing scales of the object
	glm::vec4 worldMaxBounds;
	glm::vec4 worldMinBounds;
public:
	glm::mat4 *modelMatrix;
};