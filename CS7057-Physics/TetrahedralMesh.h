#pragma once
#include "model.h"


struct FEMElement {
	//glm::vec3 position;
	int indices[4];				//Indices to te 4 vertices of this tet
	glm::mat3 localRotation;	//R
	
	float volume;

	//glm::vec3 center;
	glm::vec3 e0, e1, e2;	//edge vectors for undeformed tet

	glm::vec3 B[4];

	glm::mat3 Ke[4][4]; //Stiffness matrices
	
	//Think this actually needs to be per-vertex since a vert can belong to many tets
	//glm::mat3 Ke_Warp[4][4]; //Warped stiffness matrices

	FEMElement(int v1, int v2, int v3, int v4)
	{
		indices[0] = v1;
		indices[1] = v2;
		indices[2] = v3;
		indices[3] = v4;
	}
};

class TetrahedralMesh
{
private:
	struct Vertex
	{
		glm::vec3 X;	//Position
		glm::vec3 n;	//Vertex Normal	
		
		glm::vec3 X0;	//Initial position
		glm::vec3 n0;	//Initial vertex normal


		glm::vec3 velocity;

		float m;		//mass

		std::map<int,glm::mat3> K_Warp;	//Warped stiffness matrices for all outgoing edges
		std::map<int,glm::mat3> A;

		glm::vec3 b;

		glm::vec3 F_u;		//Force offset vector
		glm::vec3 Fext;		//External forces
		bool isFixed;

		//Variables for conjugate gradient solver
		glm::vec3 residual;
		glm::vec3 previous;
		glm::vec3 update;
		
		Vertex()
		{
			X = glm::vec3(0);
			X0 = n = n0 = velocity = b = F_u = Fext = residual = previous = update = glm::vec3(0);
			isFixed = false;
			m = 0;
		}
	};

public:
	Model *model;
	Shader *shader;
	
	//std::vector<glm::vec3> deformedVertices;

	bool isWireframe;
	bool usingWarpedStiffness;

	enum Face {LEFT, RIGHT, FRONT, BACK, TOP, BOTTOM};

	std::vector<TetrahedralMesh::Vertex> verts;
	std::vector<FEMElement> elements;	//A finite number of elements

	std::vector<GLint> tris;
	std::vector<GLint> tets;

	//Bounds For Identifying bounding faces when setting fixed points
	glm::vec3 minBounds;
	glm::vec3 maxBounds;
	
	TetrahedralMesh(void);	
	TetrahedralMesh(Model &m);
	TetrahedralMesh(std::string path, float density = 1000.0f, float massDamping = 2.0f, float stiffnessDamping = 0.0f);
	
	void Init();
	
	//Fix all vertices on one face
	void SetFixedFace(Face f);

	void ApplyConstantAcceleration(glm::vec3 f = glm::vec3(0, -9.81, 0));
	void Draw(glm::mat4 v, glm::mat4 p);
	bool LoadMesh(std::string path);
	void Update(float deltaTime);

	void GenerateDelaunayTetrahedralisation();

	~TetrahedralMesh(void);

private:
	std::vector<glm::vec2>	texCoords;
	std::vector<glm::vec3>	normals;
	std::vector<GLuint>		surfaceVerts;

	//Intended to be used to import surface normals and texcoords
	bool LoadSurfaceData(std::string path);

	//Initialisation
	bool InitialiseTetrahedra();
	bool InitialisePoints();
	void InitK_Warp(FEMElement e);
	void SetupBuffers();	//Setup VAO AND EBO for rendering
	void CalculateStiffnessMatrix(FEMElement &e);		//Calculate element stiffness matrices
	void InitialisePoint(TetrahedralMesh::Vertex &v);	//Initialise per-vertex data
	void SetPointMass(TetrahedralMesh::Vertex &v);		//Set vertex mass based on full mesh mass
	
	//Update Loop
	void UpdateTetrahedra();
	void UpdateOrientations();
	void ResetOrientations();
	void ClearWarpedStiffness();
	void CalculateWarpedStiffness(const FEMElement &e);
	void AddPlasticityForces();
	void DynamicsAssembly(float deltaTime);
	void ConjugateGradientSolver(float deltaTime);
	void UpdateVertexPositions(float deltaTime);
	void ClearForces();

	//Material Parameters
	float massDampingFactor;		 //alpha
	float stiffnessDampingFactor;	//beta
	float youngsModulus;	//(E) Essentially a measure of material elasticity
	float poissonRatio;//(v) Constant ratio across the material of transverse to longitudinal deformation
	float massDensity;	//Used as Mi based on an assumed uniform mass density.
	float totalVolume;
	float totalMass;
	//Non-zero elements of elasticity matrix
	float D0, D1, D2;

	GLuint VAO, VBO, EBO;
};