#include "BoundingSphere.h"
#include <math.h>

GLuint BoundingSphere::VBO = 0;
GLuint BoundingSphere::VAO = 0;

Shader* BoundingSphere::shader = nullptr;

BoundingSphere::BoundingSphere()
{
}

void BoundingSphere::GenerateBuffers()
{
	BoundingSphere::shader = ShaderManager::loadShader("bv_debug");

	GLfloat vertices[216];

	float twoPi = 6.283185f;
	float j = 0;
	for (int i =0; i < 70; i +=3)
	{

		vertices[i] = cos(j);
		vertices[i+1] = sin(j);
		vertices[i+2] = 0.0f;

		j = (twoPi/69.0f) * i;
	}

	j = 0;
	for (int i = 72; i < 144; i +=3)
	{

		vertices[i] = cos(j);
		vertices[i+1] = 0.0f;
		vertices[i+2] = sin(j);

		j = (twoPi/69.0f) * i;
	}

	j = 0;
	for (int i = 144; i < 214; i +=3)
	{

		vertices[i] = 0.0f;
		vertices[i+1] = sin(j);
		vertices[i+2] = cos(j);

		j = (twoPi/69.0f) * i;
	}

	/* Get GL to allocate space for our array buffers */
	glGenVertexArrays(1, &BoundingSphere::VAO);
	glGenBuffers(1, &BoundingSphere::VBO);

	/* Tell GL we're now working on our Vertex Array Object */
	glBindVertexArray(BoundingSphere::VAO);
	
	/* Give GL our vertices */
	glBindBuffer(GL_ARRAY_BUFFER, BoundingSphere::VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	/* Tell GL we don't need to work on our buffers any more */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

BoundingSphere::BoundingSphere(Model* m)
{
	isColliding = false;
	ComputeRadius(m);
	
	if (BoundingSphere::VAO == 0)	
		GenerateBuffers();
}

BoundingSphere::BoundingSphere(std::vector<Mesh::Vertex*> *verts)
{
	isColliding = false;
	ComputeRadius(verts);
	if (BoundingSphere::VAO == 0)	
		GenerateBuffers();
}

BoundingSphere::BoundingSphere(std::vector<Triangle*> &triangles)
{
	isColliding = false;
	ComputeRadius(triangles);
	if (BoundingSphere::VAO == 0)	
		GenerateBuffers();
}

BoundingSphere::BoundingSphere(glm::vec3 pos, float rad)
{
	this->position		= glm::vec4(pos, 1.0f);
	this->center		= pos;
	this->localRadius	= rad;
	this->radius		= rad;
	
	obbMinBounds = center - glm::vec3(radius);
	obbMaxBounds = center + glm::vec3(radius);

	isColliding = false;
	if (BoundingSphere::VAO == 0)	
		GenerateBuffers();
}

BoundingSphere::BoundingSphere(glm::vec3 min, glm::vec3 max)
{
	obbMinBounds = min;
	obbMaxBounds = max;

	this->center		= (obbMaxBounds + obbMinBounds)/2.0f;
	this->position		= glm::vec4(center, 1.0f);

	this->localRadius	= glm::length(obbMaxBounds - obbMinBounds)/2.0;
	this->radius		= localRadius;
	
	isColliding = false;
	if (BoundingSphere::VAO == 0)	
		GenerateBuffers();
}

void BoundingSphere::ComputeRadius(std::vector<Triangle*> &triangles)
{
	glm::vec3 maxPoint = glm::vec3(0);

	obbMaxBounds = glm::vec3(-99999999);
	obbMinBounds = glm::vec3(99999999);

	for (int i =0; i < triangles.size(); i++)
	{
		if (glm::length((triangles)[i]->centroid) > glm::length(maxPoint))
		{
			maxPoint = (triangles)[i]->centroid;
		}

		for (int j = 0; j < 3; j++)
		{

			if ((triangles)[i]->points[j].x > obbMaxBounds.x)
				obbMaxBounds.x = (triangles)[i]->points[j].x;
			if ((triangles)[i]->points[j].y > obbMaxBounds.y)
				obbMaxBounds.y = (triangles)[i]->points[j].y;
			if ((triangles)[i]->points[j].z > obbMaxBounds.z)
				obbMaxBounds.z = (triangles)[i]->points[j].z;

			if ((triangles)[i]->points[j].x < obbMinBounds.x)
				obbMinBounds.x = (triangles)[i]->points[j].x;
			if ((triangles)[i]->points[j].y < obbMinBounds.y)
				obbMinBounds.y = (triangles)[i]->points[j].y;
			if ((triangles)[i]->points[j].z < obbMinBounds.z)
				obbMinBounds.z = (triangles)[i]->points[j].z; 
		}
	}

	this->position		= glm::vec4((obbMaxBounds + obbMinBounds)/2.0f, 1.0f);
	this->center		= (obbMaxBounds + obbMinBounds)/2.0f;
	this->localRadius	= glm::length(obbMaxBounds - obbMinBounds)/2.0;
	this->radius		= localRadius;
}

void BoundingSphere::ComputeRadius(std::vector<Mesh::Vertex*> *verts)
{
	glm::vec3 maxPoint = glm::vec3(0);

	obbMaxBounds = glm::vec3(-99999999);
	obbMinBounds = glm::vec3(99999999);

	for (int i =0; i < verts->size(); i++)
	{
		if (glm::length((*verts)[i]->Position) > glm::length(maxPoint))
		{
			maxPoint = (*verts)[i]->Position;
		}

		if ((*verts)[i]->Position.x > obbMaxBounds.x)
			obbMaxBounds.x = (*verts)[i]->Position.x;
		if ((*verts)[i]->Position.y > obbMaxBounds.y)
			obbMaxBounds.y = (*verts)[i]->Position.y;
		if ((*verts)[i]->Position.z > obbMaxBounds.z)
			obbMaxBounds.z = (*verts)[i]->Position.z;

		if ((*verts)[i]->Position.x < obbMinBounds.x)
			obbMinBounds.x = (*verts)[i]->Position.x;
		if ((*verts)[i]->Position.y < obbMinBounds.y)
			obbMinBounds.y = (*verts)[i]->Position.y;
		if ((*verts)[i]->Position.z < obbMinBounds.z)
			obbMinBounds.z = (*verts)[i]->Position.z;
	}

	this->position		= glm::vec4((obbMaxBounds + obbMinBounds)/2.0f, 1.0f);
	this->center		= (obbMaxBounds + obbMinBounds)/2.0f;
	this->localRadius	= glm::length(obbMaxBounds - obbMinBounds)/2.0f;
	this->radius		= localRadius;
}

void BoundingSphere::ComputeRadius(Model* m)
{
	glm::vec3 maxPoint = glm::vec3(0);

	for (int i =0; i < m->meshes.size(); i++)
	{
		for (int j=0; j < m->meshes[i].vertices.size(); j++)
		{
			if (glm::length(m->meshes[i].vertices[j].Position) > glm::length(maxPoint))
			{
				maxPoint = m->meshes[i].vertices[j].Position;
			}
		}
	}

	this->center		= (m->maxBounds + m->minBounds) / 2.0f;
	this->localRadius	= glm::length(maxPoint - center);
	this->radius		= localRadius;
	this->position		= glm::vec4(this->center, 1.0f);
	
	obbMinBounds = center - glm::vec3(radius);
	obbMaxBounds = center + glm::vec3(radius);
}

void BoundingSphere::Update(glm::mat4 *modelMat)
{
	this->modelMatrix = modelMat;

	position = (*modelMatrix) * glm::vec4(center, 1.0f);

	//TODO - Don't assume uniform scaling
	this->radius = localRadius * glm::length((*modelMatrix)[0]);
}

void BoundingSphere::Draw(glm::mat4 v, glm::mat4 p, glm::vec3 color)
{
	glm::mat4 model = *modelMatrix;

	model[3][0] = position.x;
	model[3][1] = position.y;
	model[3][2] = position.z;

	model = glm::scale(model, glm::vec3(localRadius));


	BoundingSphere::shader->enableShader();
	BoundingSphere::shader->setUniformMatrix4fv("modelMat", model);
	BoundingSphere::shader->setUniformMatrix4fv("viewMat",		v);
	BoundingSphere::shader->setUniformMatrix4fv("projectionMat",  p);

	if (isColliding)
		BoundingSphere::shader->setUniformVector4fv("col", glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	else
		BoundingSphere::shader->setUniformVector4fv("col", glm::vec4(color, 1.0f));

	glBindVertexArray(BoundingSphere::VAO);
	
	glDrawArrays(GL_LINE_LOOP, 0, 48);
	glDrawArrays(GL_LINE_LOOP, 48, 23);
//	glDrawArrays(GL_LINE_LOOP, 48, 72);

	glBindVertexArray(0);

	BoundingSphere::shader->disableShader();
}

BoundingSphere::~BoundingSphere(void)
{
}
