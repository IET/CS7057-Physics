#pragma once

#include <glm\glm.hpp>
#include "ParticleSystem.h"
#include "PhysObject.h"
#include <vector>
#include "SphereTree.h"
#include "IntersectionTest.h"
#include "DebugOutput.h"

#define COLLISION_THRESHOLD 0.0001f

struct Contact{
	PhysObject *a;
	PhysObject *b;

	glm::vec3 p; //Contact Point
	glm::vec3 n; //Contact normal

	glm::vec3 ea; //Edge direction for a
	glm::vec3 eb; //Edge direction for b

	bool isVertexFace;	//Defines whether contact is vertex-face
};

static class Physics {

public:
	//TODO - Maybe use a map instead so that they may be retrieved by name
	static std::vector<PhysObject> PhysicsObjects;

	static bool NarrowPhase(PhysObject *obj1, PhysObject *obj2, vector<BVHNode> &collidingNodes, float deltaTime);

	static void TestCollision(BVHNode *&node1, BVHNode *&node2, vector<BVHNode> &collidingNodes);

	static bool CollisionResponse(PhysObject *obj1, PhysObject *obj2, BVHNode* n1, BVHNode* n2);
	static bool CollisionResponse(PhysObject *obj1, PhysObject *obj2, glm::vec3 &point1, glm::vec3 &point2);

	static void timestep(float dt);

	//Credit to opengl-tutorial.org
	//http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
	//OBB = Oriented Boudning Box
	//Converts mouse position to a ray in world space
	static void ScreenPosToWorldRay(
		int mouseX, int mouseY,             // Mouse position, in pixels, from bottom-left corner of the window
		int screenWidth, int screenHeight,  // Window size, in pixels
		glm::mat4 ViewMatrix,               // Camera position and orientation
		glm::mat4 ProjectionMatrix,         // Camera parameters (ratio, field of view, near and far planes)
		glm::vec3& out_origin,              // Ouput : Origin of the ray. /!\ Starts at the near plane, so if you want the ray to start at the camera's position instead, ignore this.
		glm::vec3& out_direction            // Ouput : Direction, in world space, of the ray that goes "through" the mouse.
	);

	//Credit to opengl-tutorial.org
	//http://www.opengl-tutorial.org/miscellaneous/clicking-on-objects/picking-with-custom-ray-obb-function/
	static bool TestRayOBBIntersection(
		glm::vec3 ray_origin,        // Ray origin, in world space
		glm::vec3 ray_direction,     // Ray direction (NOT target position!), in world space. Must be normalize()'d.
		glm::vec3 aabb_min,          // Minimum X,Y,Z coords of the mesh when not transformed at all.
		glm::vec3 aabb_max,          // Maximum X,Y,Z coords. Often aabb_min*-1 if your mesh is centered, but it's not always the case.
		glm::mat4 ModelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
		float& intersection_distance // Output : distance between ray_origin and the intersection with the OBB
	);
};