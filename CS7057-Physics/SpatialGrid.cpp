#include "SpatialGrid.h"
#include <math.h>
#include "IntersectionTest.h"

std::vector<std::vector<PhysObject*>> SpatialGrid::grid;

SpatialGrid::SpatialGrid(void)
{
	max = 5000;
	min = -5000;

	cellSize = 200.0f;

	collisionTests = 0;
	frames = 0;
}

SpatialGrid::SpatialGrid(float min, float max, float cell_size)
{
	collisionTests = 0;
	frames = 0;

	this->min = min;
	this->max = max;

	this->cellSize = cell_size;
	
	float width = (max-min)/cellSize;
	
	//grid.resize((int)(width * width * width), std::vector<PhysObject*>(5, nullptr));
	grid = std::vector<std::vector<PhysObject*>>((int)(width * width * width), std::vector<PhysObject*>(0, nullptr));
	
	this ->shader = ShaderManager::loadShader("bv_debug");
	int verticesPerSide = (int)(width + 0.99f);
	numVertices = (((verticesPerSide * 2 * (verticesPerSide + 1))) * 3);
	
	GLfloat* vertices = new GLfloat[numVertices * 3];


	//Lines spanning y
	for (int i =0; i < (numVertices/3) + 1; i ++)
	{
		int mod = verticesPerSide + 1;
		vertices[i * 3]			= min + (cellSize * ((i/2) % mod));
		vertices[(i * 3) + 1]	= min + ((verticesPerSide * cellSize) * (i %2));
		vertices[(i * 3) + 2]	= min +	(cellSize * (((i/2)/(verticesPerSide)) % mod));
	}
	//Lines spanning x
	for (int i = numVertices/3; i < ((numVertices*2)/3) + 1; i ++)
	{
		int mod = verticesPerSide + 1;
		vertices[i * 3]			= min + (cellSize * ((i/2) % mod));
		vertices[(i * 3) + 1]	= min +	(cellSize * (((i/2)/(verticesPerSide)) % mod));
		vertices[(i * 3) + 2]	= min + ((verticesPerSide * cellSize) * (i %2));
	}

	//Lines spanning z
	for (int i = (numVertices*2)/3; i < numVertices; i ++)
	{
		int mod = verticesPerSide + 1;
		vertices[i * 3]			= min + ((verticesPerSide * cellSize) * (i %2));
		vertices[(i * 3) + 1]	= min +	(cellSize * (((i/2)/(verticesPerSide)) % mod));
		vertices[(i * 3) + 2]	= min + (cellSize * ((i/2) % mod));
	}

	/* Get GL to allocate space for our array buffers */
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);

	/* Tell GL we're now working on our Vertex Array Object */
	glBindVertexArray(this->VAO);
	
	/* Give GL our vertices */
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, numVertices * 3 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	/* Tell GL we don't need to work on our buffers any more */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	delete[] vertices;
}

void SpatialGrid::Insert(PhysObject *p)
{
	//TODO - Guard against objects leaving the grid.
	int hash = HashFunction(p->GetPosition());
	grid[hash].push_back(p);

	int inserted = 0 | (0x01 << hash);

	//Test all the principal axes in case it spans a grid node in that dimension
	for (int i =0; i < 6; i++)
	{
		int index = 4 >> (i % 3);
		glm::vec3 axis = glm::vec3((index & 0x4) >> 2, (index & 0x2) >> 1, index & 0x1);

		if (i > 2)
			axis *= -1.0f;

		hash = HashFunction(p->GetPosition() + (p->bs.radius * axis));

		if (!(inserted & (0x01 << hash)))
		{
			grid[hash].push_back(p);
			inserted |= (0x01 << hash);
		}
	}
	//grid[inserted];
}

void SpatialGrid::Draw(glm::mat4 v, glm::mat4 p)
{
	this->shader->enableShader();
	this->shader->setUniformMatrix4fv("modelMat", glm::mat4(1));
	this->shader->setUniformMatrix4fv("viewMat",		v);
	this->shader->setUniformMatrix4fv("projectionMat",  p);
	
	this->shader->setUniformVector4fv("col", glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));

	glBindVertexArray(this->VAO);
	
	glDrawArrays(GL_LINES, 0, numVertices);
	
	glBindVertexArray(0);

	this->shader->disableShader();
}

void SpatialGrid::TestCollisions()
{
	frames += 1.0f;

	for (int i = 0; i < grid.size(); i++)
	{
		for (int j =0; j< grid[i].size(); j++)
		{
			if (grid[i][j] == nullptr)
				continue;

			for (int k=0; k < grid[i].size(); k++)
			{
				if (j == k)
					continue;
				IntersectionTest::TestIntersection(&grid[i][j]->bs, &grid[i][k]->bs);
				collisionTests += 1.0f;			
			}
		}
	}
}

void SpatialGrid::Insert(PhysObject *p, glm::vec3 finalPosition)
{
	glm::vec3 initialPosition = p->GetPosition();

	for (int i = floor(initialPosition.x); i < floor(finalPosition.x); i ++)
	{
		for (int j = floor(initialPosition.y); j < floor(finalPosition.y); j ++)
		{
			for (int k = floor(initialPosition.z); k < floor(finalPosition.z); k ++)
			{
				grid[HashFunction(glm::vec3(i, j, k))].push_back(p);
			}
		}
	}

	grid[HashFunction(p->GetPosition())].push_back(p);
}

void SpatialGrid::Reset()
{
	//Clear deletes everything but I still want my bins allocated for the grid space
	//grid.clear();
	float width = (max-min)/cellSize;

	//grid.resize(width * width * width);
	//grid = std::vector<std::vector<PhysObject*>>((int)(width * width * width), std::vector<PhysObject*>(0, nullptr));

	for (int i = 0; i < grid.size(); i++)
	{
		grid[i] = std::vector<PhysObject*>(0, nullptr);
	}
}

int SpatialGrid::HashFunction(glm::vec3 p)
{
	float width = (max - min)/cellSize;

	if (min < 0.0f)
	{
		p += (glm::vec3(min, min, min) * -1.0f);
	}

	//TODO - Optimise this function and ensure determinism
	return floor(p.x/cellSize) + (floor(p.y/cellSize) * width) + (floor(p.z/cellSize) * width * width);
}


SpatialGrid::~SpatialGrid(void)
{
	//if (vertices != nullptr)
		//delete[] vertices;
	if (frames > 0)
		std::cout << "Spatial Grid - Collision Tests Per Frame: " << (collisionTests/frames) << std::endl;
}
