#include "BVHDemo.h"
#include "TextureUtils.h"
#include "glfw3.h"

BVHDemo::BVHDemo(void)
{
}

void BVHDemo::Init()
{
	texShader = ShaderManager::loadShader("texture");
	planeModel.load("../models/plane/F-18.obj");
	planeModel.setShader(texShader);

	plane = PhysObject(planeModel, glm::vec3(0, 0.0f, 0.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	plane.setTexture(TextureUtils::LoadTextureFromFile("../textures/plane/SopCamel(C).jpg") );
	plane.bvh.Init(plane.model.triangles, MAX_DEPTH);

	treeLevel = 0;
}

void BVHDemo::Update(float dt)
{
	plane.Update(dt);
}

void BVHDemo::Draw(glm::mat4 v, glm::mat4 p)
{
	plane.Draw(v, p);
	plane.bvh.Draw(v, p, treeLevel);
}

void BVHDemo::Cleanup()
{

}

void BVHDemo::HandleInput(bool *keys)
{	
	if(keys[GLFW_KEY_UP])	
	{
		if(treeLevel < MAX_DEPTH)
			treeLevel++;
		keys[GLFW_KEY_UP] = false;
	}

	if(keys[GLFW_KEY_DOWN])	
	{
		if (treeLevel > 0)
			treeLevel--;
		keys[GLFW_KEY_DOWN] = false;
	}
}

BVHDemo::~BVHDemo(void)
{
}
