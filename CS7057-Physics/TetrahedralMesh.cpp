#include "TetrahedralMesh.h"
#include <iostream>
#include <fstream>
#include <regex>

//Credit to Kenny Erleben's Book, Physics-Based animation on which a lot of this is based

//TODO - Move the FEM physics stuff out to another class so that volumetric meshes may be used with other deformation models

TetrahedralMesh::TetrahedralMesh(void)
{
	minBounds = glm::vec3(std::numeric_limits<float>::max());
	maxBounds = glm::vec3(-std::numeric_limits<float>::max());

	massDampingFactor = 0.2f;
	stiffnessDampingFactor = 0.1f;

	massDensity = 100.0f;

	poissonRatio = 0.33f;

	youngsModulus = 500000.0f;

	//Constant factor of D
	float d = youngsModulus / ((1.0f + poissonRatio) * (1.0f - 2 * poissonRatio));
	//Non-zero elements of elasticity matrix D P.351 (10.73)
	D0 = (1.0f - poissonRatio) * d;
	D1 = poissonRatio * d;
	D2 = youngsModulus / (2 * (1.0f + poissonRatio));

	isWireframe = true;
	usingWarpedStiffness = true;
}


TetrahedralMesh::TetrahedralMesh(std::string path, float density, float massDamping, float stiffnessDamping)
{
	minBounds = glm::vec3(std::numeric_limits<float>::max());
	maxBounds = glm::vec3(-std::numeric_limits<float>::max());

	poissonRatio = 0.33f;
	youngsModulus = 500000.0f;

	//Constant factor of D
	float d = youngsModulus / ((1.0f + poissonRatio) * (1.0f - 2 * poissonRatio));
	//Non-zero elements of elasticity matrix D P.351 (10.73)
	D0 = (1.0f - poissonRatio) * d;
	D1 = poissonRatio * d;
	D2 = youngsModulus / (2 * (1.0f + poissonRatio));

	LoadMesh(path);

	massDampingFactor = massDamping;
	stiffnessDampingFactor = stiffnessDamping;

	massDensity = density;
	InitialiseTetrahedra();
	InitialisePoints();
	SetupBuffers();
}

void TetrahedralMesh::Init()
{
	InitialiseTetrahedra();
	InitialisePoints();
	SetupBuffers();
}

void TetrahedralMesh::SetFixedFace(Face f)
{
	glm::vec3 comparator(0);

	switch (f)
	{
	case FRONT:
		comparator.z = maxBounds.z;
		break;
	case BACK:
		comparator.z = minBounds.z;
		break;
	case LEFT:
		comparator.x = minBounds.x;
		break;
	case RIGHT:
		comparator.x = maxBounds.x;
		break;
	case TOP:
		comparator.y = maxBounds.y;
		break;
	case BOTTOM:
		comparator.y = minBounds.y;
		break;
	default:
		return;
	}

	for (int i = 0; i < verts.size(); i++)
	{
		Vertex v = verts[i];
		glm::vec3 vertComponent = glm::normalize(comparator) * glm::normalize(comparator) * verts[i].X;
		float magDiff = glm::length(vertComponent - comparator);

		if ( magDiff < 0.001f && magDiff > -0.001f)
			verts[i].isFixed = true;
	}
}

void TetrahedralMesh::Draw(glm::mat4 v, glm::mat4 p)
{
	// Draw mesh
	glBindVertexArray(this->VAO);

	this->shader->enableShader();
	glm::mat4 mod = glm::mat4(20);
	mod[3][3] = 1.0f;

	this->shader->setUniformMatrix4fv("projectionMat", p);
	this->shader->setUniformMatrix4fv("viewMat", v);
	this->shader->setUniformMatrix4fv("modelMat", mod);

	//Wait, am I filling the buffer with vertex objects?
	//That's a huge object...
	//This was a terrible idea...
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, verts.size() * sizeof(TetrahedralMesh::Vertex), &verts[0]);

	if (isWireframe)
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	
	glDrawElements(GL_TRIANGLES, this->tris.size(), GL_UNSIGNED_INT, 0);
	
	if (isWireframe)
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	glBindVertexArray(0);
	
	this->shader->disableShader();
}

//This is probably not the most efficient loader but I found that .mesh files
//aren't necessarily the most consitent so I wanted to keep it flexible
bool TetrahedralMesh::LoadMesh(std::string path)
{
	std::ifstream in(path);

	//If this is not a .mesh file
	if (0 != path.compare (path.length() - 5, path.length(), ".mesh"))
	{
		std::cout << "Specified Tetrahedral Mesh File Does Not Have Extension .mesh";
		return false;
	}

	enum inputMode {PREAMBLE, VERTICES, TETS, TRIS, END};
	int  mode = 0;

	std::string vertHeading	= "Vertices";
	std::string tetHeading	= "Tetrahedra";
	std::string triHeading	= "Triangles";
	
	if (!in)
	{
		std::cout << "Error opening file: " << path << std::endl;
		return false;
	}

	std::string line;
	while ( getline(in, line))
	{
		//Ignore comments
		if (line[0] == '#')
			continue;

		//Outside of preamble, endline means the end of a section
		if (line == "" || line == "\r\n" || line == "\n")
		{
			if (mode != inputMode::PREAMBLE)
				mode++;
			continue;
		}

		if (std::regex_match(line, std::regex("^[[:alpha:]]+$")))
		{
			if (line == vertHeading)
				mode = inputMode::VERTICES;
			else if (line == tetHeading)
				mode = inputMode::TETS;
			else if (line == triHeading)
				mode = inputMode::TRIS;
			else
				//Default to preamble so as not to overwrite any data already read
				mode = inputMode::PREAMBLE;
			continue;
		}		

		//Invalid mode, we're done here
		if (mode >= inputMode::END)
			break;

		//If it's just one integer, this is the start of a section and defines the number of items in the section
		if (std::regex_match(line, std::regex("^\\d+$")))
		{
			//This section not needed as memory is not being allocated ahead of time
			continue;
		}

		//Don't try to read data in preamble mode
		if (mode == inputMode::PREAMBLE)
			continue;

		//Read data
		std::vector<std::string> items;
		std::stringstream dataStream(line);
		std::string item;

		while(getline(dataStream, item, ' '))
		{
			if (item.length() > 0)
				items.push_back(item);
		}

		int a, b, c, d;

		TetrahedralMesh::Vertex vert;

		switch(mode)
		{
			case inputMode::VERTICES:
				vert.X = vert.X0 = glm::vec3(atoi(items[0].c_str()), atoi(items[1].c_str()), atoi(items[2].c_str()));
				verts.push_back(vert);

				if (vert.X.x < minBounds.x)
					minBounds.x = vert.X.x;
				if (vert.X.y < minBounds.y)
					minBounds.y = vert.X.y;
				if (vert.X.z < minBounds.z)
					minBounds.z = vert.X.z;

				if (vert.X.x > maxBounds.x)
					maxBounds.x = vert.X.x;
				if (vert.X.y > maxBounds.y)
					maxBounds.y = vert.X.y;
				if (vert.X.z > maxBounds.z)
					maxBounds.z = vert.X.z;

				break;
			case inputMode::TETS:

				if (items[0] == "0")
				{
					a = atoi(items[1].c_str()) - 1;
					b = atoi(items[2].c_str()) - 1;
					c = atoi(items[3].c_str()) - 1;
					d = atoi(items[4].c_str()) - 1;
				}
				else
				{
					a = atoi(items[0].c_str()) - 1;
					b = atoi(items[1].c_str()) - 1;
					c = atoi(items[2].c_str()) - 1;
					d = atoi(items[3].c_str()) - 1;
				}

				tets.push_back(a);
				tets.push_back(b);
				tets.push_back(c);
				tets.push_back(d);

				elements.push_back(FEMElement(a, b, c, d));

				break;
			case inputMode::TRIS:
				if (items[0] == "0" || items[0] == "1")
				{
					a = atoi(items[1].c_str());
					b = atoi(items[2].c_str());
					c = atoi(items[3].c_str());
				}
				else
				{		
					a = atoi(items[0].c_str());
					b = atoi(items[1].c_str());
					c = atoi(items[2].c_str());
				}

				tris.push_back(a - 1);
				tris.push_back(b - 1);
				tris.push_back(c - 1);

				break;
			default:
				break;
		}
		
	}
	
	Init();
	return true;
}

//Gravity is a nice simple force, it acts equally on all vertices
void TetrahedralMesh::ApplyConstantAcceleration(glm::vec3 f)
{
	for (int i =0;i < verts.size(); i++)
	{
		verts[i].Fext += f * verts[i].m;
	}
}

void TetrahedralMesh::UpdateTetrahedra()
{
	//Reset warped stiffness matrices
	ClearWarpedStiffness();

	//Update Orientation Matrices
	if (usingWarpedStiffness)
		UpdateOrientations();
	else
		ResetOrientations();

	for (int i = 0; i < elements.size(); i++)
		CalculateWarpedStiffness(elements[i]);
}

void TetrahedralMesh::UpdateOrientations()
{
	for (int i = 0; i < elements.size(); i++)
	{
		//TODO - Remove this line. Just laziness since this method used to take an element as input
		FEMElement& e = elements[i];

		//TODO - Consider storing this if you're using it often (or at least store Jdet since that was used to get volume in the first place)
		float invJdet = 1.0f / (6 * e.volume);
		
		//Current edge vectors of tet
		glm::vec3 e0 = verts[e.indices[1]].X - verts[e.indices[0]].X;
		glm::vec3 e1 = verts[e.indices[2]].X - verts[e.indices[0]].X;
		glm::vec3 e2 = verts[e.indices[3]].X - verts[e.indices[0]].X;
	
		//P364 (10.131)
		glm::vec3 n1 = glm::cross(e1, e2) * invJdet;
		glm::vec3 n2 = glm::cross(e2, e0) * invJdet;
		glm::vec3 n3 = glm::cross(e0, e1) * invJdet;

		//Undeformed edge vectors of tet
		e0 = e.e0;
		e1 = e.e1;
		e2 = e.e2;

		glm::mat3 E = glm::mat3 (e0, e1, e2);
		glm::mat3 N = glm::mat3(n1, n2, n3);

		e.localRotation = E * N;

		e.localRotation[0][0] = e0.x * n1.x + e1.x * n2.x + e2.x * n3.x;  
		e.localRotation[0][1] = e0.x * n1.y + e1.x * n2.y + e2.x * n3.y;   
		e.localRotation[0][2] = e0.x * n1.z + e1.x * n2.z + e2.x * n3.z;

		e.localRotation[1][0] = e0.y * n1.x + e1.y * n2.x + e2.y * n3.x;  
		e.localRotation[1][1] = e0.y * n1.y + e1.y * n2.y + e2.y * n3.y;   
		e.localRotation[1][2] = e0.y * n1.z + e1.y * n2.z + e2.y * n3.z;

		e.localRotation[2][0] = e0.z * n1.x + e1.z * n2.x + e2.z * n3.x;  
		e.localRotation[2][1] = e0.z * n1.y + e1.z * n2.y + e2.z * n3.y;  
		e.localRotation[2][2] = e0.z * n1.z + e1.z * n2.z + e2.z * n3.z;

		//Re-normalise and orthogonalise rotation matrix (taken from rigidbody code)
		e.localRotation[0] = glm::normalize(e.localRotation[0]);
		e.localRotation[1] = glm::cross(e.localRotation[2], e.localRotation[0]);
		e.localRotation[1] = glm::normalize(e.localRotation[1]);
		e.localRotation[2] = glm::cross(e.localRotation[0], e.localRotation[1]);
		e.localRotation[2] = glm::normalize(e.localRotation[2]);
	}
}

void TetrahedralMesh::ResetOrientations()
{
	for (int i = 0; i < elements.size(); i++)
		elements[i].localRotation = glm::mat3(1);
}


void TetrahedralMesh::CalculateWarpedStiffness(const FEMElement &e)
{
	//K' = R K R^-1
	glm::mat3 ReT = glm::transpose(e.localRotation);
	
	//Combination of (10.11 & 10.22) P.361
	for (int i =0; i < 4; i++)
	{
		glm::vec3 f = glm::vec3(0);

		for (int j =0; j < 4; j++)
		{
			f += e.Ke[i][j] *  verts[e.indices[j]].X0;

			if (j >= i)
			{
				glm::mat3 tmp = e.localRotation * e.Ke[i][j] * ReT;
				verts[e.indices[i]].K_Warp[e.indices[j]] += tmp;

				if (verts[e.indices[i]].K_Warp[e.indices[j]] == glm::mat3(0))
					assert(0);

				if(j > i)
				{
					verts[e.indices[j]].K_Warp[e.indices[i]] += glm::transpose(tmp);
				}
			}

		}
		verts[e.indices[i]].F_u -= e.localRotation * f;
	}
}

void TetrahedralMesh::AddPlasticityForces()
{
	//F_plastic = Re * Ke * Ue_plastic

	//F_plastic = Re * Pe * e_plastic

	//Pe_plastic = Ve * Transpose(B) 

	//TODO - Consider saving the B matrices from earlier
	for (int i = 0; i < elements.size(); i++)	
	{
				
	}
}

void TetrahedralMesh::DynamicsAssembly(float deltaTime)
{
	//P.366 S 10.4
	float dt2 = deltaTime * deltaTime;

	for (int i = 0; i < verts.size(); i++)
	{
		verts[i].b = glm::vec3(0);
		float m_i = verts[i].m;

		for (auto &iterator : verts[i].K_Warp)
		{
			int j = iterator.first;
			glm::mat3 Kij = iterator.second;
			
			verts[i].A[j] = Kij * dt2;

			float massDamping = m_i + (massDampingFactor * m_i * deltaTime);

			glm::vec3 Xi = Kij * verts[j].X;

			verts[i].b -= Xi;
			 
			if (i == j)
			{
				verts[i].A[j][0][0] += massDamping;
				verts[i].A[j][1][1] += massDamping;
				verts[i].A[j][2][2] += massDamping;

				if (stiffnessDampingFactor > 0)
				{
					glm::mat3 stiffnessDamping = Kij * stiffnessDampingFactor * dt2;
					verts[i].A[j] += stiffnessDamping;
				}
			}
		}
		
		verts[i].b *= deltaTime;
		verts[i].b += verts[i].velocity * m_i;
		verts[i].b -= deltaTime * (verts[i].F_u - verts[i].Fext);
	}
}

//Tried using Eigen for this but it was way too slow.
void TetrahedralMesh::ConjugateGradientSolver(float deltaTime)
{
	int maxIterations = 20;
	float minValue = 0.000000000001f;
	float threshold = 0.0001f;

	for (int i = 0; i < verts.size(); i++)
	{
		//Honestly, this is just to save me some typing...
		TetrahedralMesh::Vertex &v = verts[i];

		if (v.isFixed)
			continue;
		
		v.residual = v.b;
		
		for (auto iterator = v.A.begin(); iterator != v.A.end(); ++iterator)
		{
			int j = iterator->first;
			glm::mat3 &A = iterator->second;

			v.residual -= A * v.velocity;
		}
		v.previous = v.residual;
	}

	for (int i =0; i < maxIterations; i++)
	{
		float d0 = 0, d1 = 0;

		//TODO - consider changing this since i is vertex index literally everywhere else
		for (int j = 0; j < verts.size(); j++)
		{
			TetrahedralMesh::Vertex &v = verts[j];
	
			if (v.isFixed)
				continue;

			v.update = glm::vec3(0);

			for (auto iterator = v.A.begin(); iterator != v.A.end(); ++iterator)
			{
				int k = iterator->first;
				glm::mat3 &A = iterator->second;

				verts[k].update += A * verts[k].previous;
			}

			d0 += glm::dot(v.residual, v.residual);
			d1 += glm::dot(v.previous, v.update);
		}
		
		if (fabs(d1) < minValue)
			d1 = minValue;

		float d2 = d0/d1;
		float d3 = 0;

		for (int j = 0; j < verts.size(); j++)
		{
			if (verts[j].isFixed)
				continue;
			
			verts[j].velocity += verts[j].previous * d2;
			verts[j].residual -= verts[j].update * d2;
			d3 += glm::dot(verts[j].residual, verts[j].residual);
		}


		if (d1 < threshold)
			break;

		if (fabs(d0) < minValue)
			d0 = minValue;

		float d4 = d3/d0;

		for (int j = 0; j < verts.size(); j++)
		{
			if (verts[j].isFixed)
				continue;
			verts[j].previous = verts[j].residual + verts[j].previous * d4;
		}
	}
}

void TetrahedralMesh::UpdateVertexPositions(float deltaTime)
{
	for(int i = 0; i < verts.size(); i++)
	{
		if (verts[i].isFixed)
			continue;
		verts[i].X += deltaTime * verts[i].velocity;
	}
}

void TetrahedralMesh::ClearForces()
{
	for (int i = 0; i < verts.size(); i++)
	{
		verts[i].F_u = glm::vec3(0);
		verts[i].Fext = glm::vec3(0);
	}
}

void TetrahedralMesh::ClearWarpedStiffness()
{
	for (int i = 0; i < verts.size(); i++)
	{
		for (auto& iterator : verts[i].K_Warp)
		{
			iterator.second = glm::mat3(0);
		}
	}
}

void TetrahedralMesh::Update(float deltaTime)
{
	UpdateTetrahedra();

	DynamicsAssembly(deltaTime);

	// TODO - calculate plastic forces, as in Erleben P.264 - 366
	//For now it's not really needed since demo doesn't need to permanently deform the object.

	ConjugateGradientSolver(deltaTime);

	UpdateVertexPositions(deltaTime);
	
	ClearForces();
}

//TODO - Implememt the below because the tris in the .mesh file seem to define more than just the surface
//TODO - Write this method to load texcoords and normals from an obj or similar file
bool TetrahedralMesh::LoadSurfaceData(std::string path)
{
	Model m;
	m.load(path);
	

	assert(0);

	return false;
}

//TODO - Come back to this
TetrahedralMesh::TetrahedralMesh(Model &m)
{
	this->model = &m;
	std::vector<glm::vec3> pointList;
	std::vector<int[3]> faceList;

	//TODO - First get vertices in random order
	//Maybe make a full copy to avoid cache thrashing
	for (int i = 0; i < m.triangles.size(); i++)
	{
		for (int j = 0; j < m.meshes[i].vertices.size(); j++)
		{
			pointList.push_back(m.meshes[i].vertices[j].Position);
		}
	}

	for (int i = 0; i < m.triangles.size(); i++)
	{
		for (int j = 0; j < m.meshes[i].vertices.size(); j++)
		{
//			int currFace[3] = {m.meshes[i].indices[j *3], m.meshes[i].indices[j *3 + 1], m.meshes[i].indices[j *3 + 3]};
//			faceList.push_back( currFace );
		}
	}

	//then incrflipdelaunay
}

void TetrahedralMesh::SetupBuffers()
{
	// Create buffers/arrays
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	// Load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	
	
	//TODO - don't store vertices in such a huge struct, that's just silly
	glBufferData(GL_ARRAY_BUFFER, this->verts.size() * sizeof(TetrahedralMesh::Vertex), &this->verts[0], GL_STREAM_DRAW);  

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->tris.size() * sizeof(GLint), &this->tris[0], GL_STATIC_DRAW);
	
	// Set the vertex attribute pointers
	// Vertex Positions
	glEnableVertexAttribArray(0);	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(TetrahedralMesh::Vertex), (GLvoid*)0);

	glBindVertexArray(0);
}

bool TetrahedralMesh::InitialiseTetrahedra()
{
	//Reset Volume, it will be caclulated from the tetrahedra volumes
	totalVolume = 0;

	for (int i = 0; i < elements.size(); i++)
	{
		//elements[i].center = (verts[elements[i].indices[0]].X + verts[elements[i].indices[1]].X + verts[elements[i].indices[2]].X + verts[elements[i].indices[3]].X)/4.0f;

		//Setup edge vectors
		elements[i].e0 = verts[elements[i].indices[1]].X0 - verts[elements[i].indices[0]].X0;
		elements[i].e1 = verts[elements[i].indices[2]].X0 - verts[elements[i].indices[0]].X0;
		elements[i].e2 = verts[elements[i].indices[3]].X0 - verts[elements[i].indices[0]].X0;


		//Calculate Stiffness Matrix
		//Following method from here: http://www.colorado.edu/engineering/CAS/courses.d/AFEM.d/AFEM.Ch09.d/AFEM.Ch09.pdf
		float Jdet = glm::dot( elements[i].e0, glm::cross(elements[i].e1, elements[i].e2)); //Determinant of jacobian

		float volume = Jdet / 6; 

		//If volume =0, vertices are coplanar and this is a filthy degenerate tetrahedron
		if (volume < 0.000001f && volume > -0.000001f)
			assert(0);

		if (volume < 0.0f)
			volume *= -1.0f;

		elements[i].volume = volume;
		//Total volume of mesh, just used for calculating mass later
		totalVolume += elements[i].volume;
		//TODO - Consider making these methods of a FEMElement Class (but they'll need access to material properties of mesh)
		CalculateStiffnessMatrix(elements[i]);
		InitK_Warp(elements[i]);		
	}
	totalMass = totalVolume * massDensity;
	return true;
}

bool TetrahedralMesh::InitialisePoints()
{
	for (int i = 0; i < verts.size(); i++)
	{
		InitialisePoint(verts[i]);
		SetPointMass(verts[i]);
	}
	return true;
}

//Just allocating all of the memory and setting matrices initially to 0
void TetrahedralMesh::InitK_Warp(FEMElement e)
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			verts[e.indices[i]].K_Warp[e.indices[j]] = glm::mat3(0);

}

void TetrahedralMesh::InitialisePoint(TetrahedralMesh::Vertex &v)
{
	v.Fext = glm::vec3(0);
	v.F_u = glm::vec3(0);
	v.velocity = glm::vec3(0);
	v.X = v.X0;
}

void TetrahedralMesh::SetPointMass(TetrahedralMesh::Vertex &v)
{
	//Give fixed points infinite mass
	if (v.isFixed)
		v.m = std::numeric_limits<float>::max();
	else
		//Assume isotropic material => all points contribute equally to mass
		v.m = totalMass * (1.0f / verts.size());
}

void TetrahedralMesh::CalculateStiffnessMatrix(FEMElement &e)
{
	glm::vec3 p0 = verts[e.indices[0]].X0;
	glm::vec3 p1 = verts[e.indices[1]].X0;
	glm::vec3 p2 = verts[e.indices[2]].X0;
	glm::vec3 p3 = verts[e.indices[3]].X0;

	glm::vec3 e0 = p1 - p0;
	glm::vec3 e1 = p2 - p0;
	glm::vec3 e2 = p3 - p0;

	//Matrix E P.345 (10.32)
	glm::mat3 E = glm::mat3 (e0, e1, e2);

	float detE = glm::determinant(E);
	float invDetE = 1.0f / detE;
	
	// d/dx N0
	float B30 = (e0.z*e1.y - e0.y*e1.z) * invDetE;
	float B20 = (e0.y*e2.z - e0.z*e2.y) * invDetE;
	float B10 = (e1.z*e2.y - e1.y*e2.z) * invDetE;
	float B00 = -B10 - B20 - B30;

	// d/dy N0
	float B31 = (e0.x*e1.z - e0.z*e1.x) * invDetE;
	float B21 = (e0.z*e2.x - e0.x*e2.z) * invDetE;
	float B11 = (e1.x*e2.z - e1.z*e2.x) * invDetE;
	float B01 = -B11 - B21 - B31;
	
	// d/dz N0
	float B32 = (e0.y*e1.x - e0.x*e1.y) * invDetE;
	float B22 = (e0.x*e2.y - e0.y*e2.x) * invDetE;
	float B12 = (e1.y*e2.x - e1.x*e2.y) * invDetE;
	float B02 = -B12 - B22 - B32;

	e.B[0] = glm::vec3(B00, B01, B02);	
	e.B[1] = glm::vec3(B10, B11, B12);
	e.B[2] = glm::vec3(B20, B21, B22);
	e.B[3] = glm::vec3(B30, B31, B32);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			float bn = e.B[i].x;			
			float cn = e.B[i].y;
			float dn = e.B[i].z;

			float bm = e.B[j].x;			
			float cm = e.B[j].y;
			float dm = e.B[j].z;

			//Ke = B^T D B V^e P.351 (10.74b)
			e.Ke[i][j][0][0] = (D0 * bn * bm) + (D2 * cn * cm + dn * dm);
			e.Ke[i][j][0][1] = (D1 * bn * cm) + (D2 * cn * bm);
			e.Ke[i][j][0][2] = (D1 * bn * dm) + (D2 * dn * bm);

			e.Ke[i][j][1][0] = (D1 * cn * bm) + (D2 * bn * cm);	
			e.Ke[i][j][1][1] = (D0 * cn * cm) + (D2 * bn * bm + dn * dm);	
			e.Ke[i][j][1][2] = (D1 * cn * dm) + (D2 * dn * cm);
		
			e.Ke[i][j][2][0] = (D1 * dn * bm) + (D2 * bn * dm);	
			e.Ke[i][j][2][1] = (D1 * dn * cm) + (D2 * cn * dm);
			e.Ke[i][j][2][2] = (D0 * dn * dm) + (D2 * bn * bm + cn * cm);	
			
			e.Ke[i][j] *= e.volume;

			if (e.Ke[i][j] == glm::mat3(0))
				assert(0);
		}
	}
}

void TetrahedralMesh::GenerateDelaunayTetrahedralisation()
{}

TetrahedralMesh::~TetrahedralMesh(void)
{
}
