#pragma once

#include "Object.h"
//#include "Physics.h"
//#include "ParticleSystem.h"
#include "OBB.h"
#include "BoundingSphere.h"
#include "Octree.h"

class PhysObject : public Object{

public:
	//TODO - try get this setup to actually work
	static std::vector<PhysObject*> PhysObjects;

	OBB bv;
	BoundingSphere bs;
	Octree bvh;

	PhysObject();
	PhysObject(const char* filepath);
	PhysObject(Model m);
	PhysObject(Model m, glm::vec3 location);
	PhysObject(Model m, glm::vec3 position, glm::vec3 rotation, glm::vec3 linearMomentum, glm::vec3 angularMomentum);

	void DrawBVH(glm::mat4 v,glm::mat4 p, int level);
	
	glm::vec3 linearMomentum;
	glm::vec3 angularMomentum;
	
	glm::vec3 centreOfMass;

	float dragCoefficient;

	glm::vec3 netForce;
	glm::vec3 netTorque;

	float inverseMass;

	bool isCollider;

	void ApplyForceLocal(glm::vec3 f, glm::vec3 point);
	void ApplyForce(glm::vec3 f, glm::vec3 point);
	void ApplyForce(glm::vec3 (*f) (glm::vec3));
	void ApplyForce(glm::vec3 (*f) (glm::vec3), glm::vec3 offset);

	void Stop();

	void Update(float timestep);
	
	void Translate(glm::vec3 t);


	void SetScale(glm::vec3 scaleIn);

	float GetRestitutionCoeffcient();
	glm::mat3 GetInverseInertiaTensor();
	glm::vec3 GetCentreOfMass();
	
	glm::vec3 GetVelocity();
	glm::vec3 GetAngularVelocity();

	~PhysObject();
private:
	void Init();
	float restitutionCoeffcient;
	glm::mat3 inverseInertiaTensor;
};

