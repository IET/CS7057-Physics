#pragma once
#include "demobase.h"

class MotionDemo :
	public DemoBase
{
private:
	Shader *texShader;
	Model rectangle;

public:
	MotionDemo(void);

	void Init();

	void Update(float dt);

	void Draw(glm::mat4 v, glm::mat4 p);
	
	void Cleanup();

	~MotionDemo(void);
};

