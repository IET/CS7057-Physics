#pragma once
#include "demobase.h"
#include "InstancedModel.h"
#include "ParticleSystem.h"
#include "ParticleForces.h"

class ParticleDemo :
	public DemoBase
{
private:
	//Maximum number of particles in system
	static const int NUM_PARTICLES = 250000;
	InstancedModel particles;
	ParticleSystem particleSystem;
	PhysObject box;
	Shader *texShader;
	Model rectangle;
	float t;

public:
	ParticleDemo(void);

	void Init();

	void Update(float dt);

	void Draw(glm::mat4 v, glm::mat4 p);
	
	void Cleanup();
	
	~ParticleDemo(void);
};

