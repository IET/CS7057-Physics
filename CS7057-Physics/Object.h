#pragma once

#include "model.h"

#pragma once

class Object {
public:
	Object();
//	Object(const char* filepath);
	Object(Model m);
	Object(Model m, glm::vec3 location);

	void Draw(glm::mat4 view, glm::mat4 proj);
	static void DrawObjects(glm::mat4 view, glm::mat4 proj);

	glm::vec3 GetPosition();
	glm::vec3 GetRotation();
	glm::mat4 GetModelMatrix();
	glm::mat3 GetOrientationMatrix();

	void setTexture(GLint handle);

	Model model;

	//TODO - Does a PhysObj actually support these?
	void Translate(glm::vec3 translateBy);
	void Rotate(glm::vec3 rotateBy);
	void Scale(glm::vec3 scaleIn);	

	void SetScale(glm::vec3 scaleIn);	

	~Object();
private:
	static std::vector<Object*> Objects;
protected:
	GLint texture;

	glm::vec3 rotation;

	glm::vec3 scaleFactor;
	
public:
	glm::vec3 position;
	glm::mat4 modelMatrix;

};
