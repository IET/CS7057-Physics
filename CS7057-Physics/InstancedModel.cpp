#pragma once

#include "InstancedModel.h"

InstancedModel::InstancedModel()
{
	this->instances = 1000;
	gammaCorrection = false;
	
	positions = new glm::vec3 [instances];

}

InstancedModel::InstancedModel(int n)
{
	instances = n;
	gammaCorrection = false;

	positions = new glm::vec3[instances];
}

//Getters and Setters
int InstancedModel::getInstances()
{
	return this->instances;
}

// Constructor, expects a filepath to a 3D model. Allows instanced rendering.
InstancedModel::InstancedModel(std::string const & path, int instances, bool gamma)
{
	gammaCorrection = gamma;
	this->instances = instances;
	positions = new glm::vec3[instances];

	this->loadModel(path);
	ilInit();

	//TODO - Allow different instanced shaders to be used
	this->setShader(ShaderManager::loadShader("instanced"));
}

InstancedModel::InstancedModel(const InstancedModel &other)
	:Model(other) //Copy data from base class
{
	instances = other.instances;
	positions = new glm::vec3[instances];
	gammaCorrection = other.gammaCorrection;

	for (InstancedMesh mesh : other.meshes)
		meshes.push_back(mesh);
	
	directory = other.directory;
	maxBounds = other.maxBounds;
	minBounds = other.minBounds;
	scaleFactor = other.scaleFactor;
}

InstancedModel &InstancedModel::operator=(const InstancedModel &other)
{
	//Copy data from base class
	Model::operator=(other);

	instances = other.instances;
	positions = new glm::vec3[instances];
	for (int i = 0; i < instances; ++i)
		positions[i] = other.positions[i];

	gammaCorrection = other.gammaCorrection;

	for (auto &mesh : other.meshes)
	{
		meshes.push_back(mesh);
		meshes.back().positions = positions;
	}
	directory = other.directory;
	maxBounds = other.maxBounds;
	minBounds = other.minBounds;
	scaleFactor = other.scaleFactor;

	return *this;
}

void InstancedModel::Draw() {
	this->getShader()->enableShader();
	
	for(GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw();
	
	this->getShader()->disableShader();
}

void InstancedModel::Draw(glm::mat4 &v, glm::mat4 &p) {
	this->getShader()->enableShader();

	this->getShader()->setUniformMatrix4fv("view", v);
	this->getShader()->setUniformMatrix4fv("projection", p);
	
	for(GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw();
	
	this->getShader()->disableShader();
}

void InstancedModel::loadModel(std::string path)
{
	minBounds = glm::vec3(100000000.0f, 100000000.0f, 100000000.0f);
	maxBounds = glm::vec3(-100000000.0f, -100000000.0f, -100000000.0f);
		
	// Read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	// Check for errors
	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return;
	}
	// Retrieve the directory path of the filepath
	this->directory = path.substr(0, path.find_last_of('/'));

	// Process ASSIMP's root node recursively
	this->processNode(scene->mRootNode, scene);
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void InstancedModel::processNode(aiNode* node, const aiScene* scene)
{
	// Process each mesh located at the current node
	for(GLuint i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]]; 
		this->meshes.push_back(this->processMesh(mesh, scene));			
	}
	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for(GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene);
	}

}

InstancedMesh InstancedModel::processMesh(aiMesh* mesh, const aiScene* scene)
{
	// Data to fill
	std::vector<Mesh::Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	// Walk through each of the mesh's vertices
	for(GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Mesh::Vertex vertex;
		glm::vec3 vector; // We declare a placeholder std::vector since assimp uses its own std::vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
		// Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.Position = vector;

		//Check bounds
		for (int j = 0; j < 3; j++) {
			if (vector[j] < minBounds[j])
				minBounds[j] = vector[j];
			if (vector[j] > maxBounds[j])
				maxBounds[j] = vector[j];
		}
		// Normals
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.Normal = vector;
		// Texture Coordinates
		if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x; 
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			
		if (mesh->mTangents != NULL) {
			// Tangent
			vector.x = mesh->mTangents[i].x;
			vector.y = mesh->mTangents[i].y;
			vector.z = mesh->mTangents[i].z;
			vertex.Tangent = vector;
			// Bitangent
			vector.x = mesh->mBitangents[i].x;
			vector.y = mesh->mBitangents[i].y;
			vector.z = mesh->mBitangents[i].z;
			vertex.Bitangent = vector;
		}
		else
		{
			vertex.Tangent = glm::vec3(0.0f, 0.0f, 0.0f);				
			vertex.Bitangent = glm::vec3(0.0f, 0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for(GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices std::vector
		for(GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// Process materials
	if(mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. Height maps
		vector<Texture> heightMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}
		
	// Return a mesh object created from the extracted mesh data
	return InstancedMesh(vertices, indices, textures, instances, positions);
}

InstancedModel::~InstancedModel(){

	//if (positions != nullptr)
		//delete[] positions;
		//positions = nullptr;
}