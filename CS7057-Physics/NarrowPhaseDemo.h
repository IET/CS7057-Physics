#pragma once
#include "demobase.h"

class NarrowPhaseDemo :
	public DemoBase
{
private:
	Shader *texShader;
	Model rectangle;
	vector<BVHNode> collidingNodes;

public:
	NarrowPhaseDemo(void);

	void Init();

	void Update(float dt);

	void Draw(glm::mat4 v, glm::mat4 p);
	
	void HandleInput(bool *keys);

	void Cleanup();

	~NarrowPhaseDemo(void);
};

