#pragma once

#include "model.h"
#include "BoundingSphere.h"
#include "PhysObject.h"
#include "OBB.h"

class IntersectionTest
{
public:
	//TODO - Make this actually test for intersection over the triangle area rather than just the points
	static bool TestIntersection(BoundingSphere* s, Triangle *t);
	static bool TestIntersection(BoundingSphere *s1, BoundingSphere *s2);
	static bool TestIntersection(BoundingSphere *s1, BoundingSphere *s2, glm::mat4 &m1, glm::mat4 &m2);
	static bool TestIntersection(BoundingSphere *s1, BoundingSphere *s2, float deltaTime);

	//static bool TestCollision(BVHNode *node1, BVHNode *node2, BVHNode *&out1, BVHNode *&out2);

	static bool TestIntersection(OBB* a, OBB *b);

	static bool TestIntersection(OBB* a, BoundingSphere *b);
	static bool TestIntersection(BoundingSphere *b, OBB* a);

	static bool TestIntersection(Triangle* a, Triangle *b);

	//The below pre-processor defines are from http://web.archive.org/web/19990203013328/http://www.acm.org/jgt/papers/Moller97/tritri.html
	//Added here for speed of implementation

	//TODO - Remove horrible preprocessor heavy code

	/* this edge to edge test is based on Franlin Antonio's gem:
	   "Faster Line Segment Intersection", in Graphics Gems III,
	   pp. 199-202 */ 
	#define EDGE_EDGE_TEST(V0,U0,U1)                      \
	  Bx=U0[i0]-U1[i0];                                   \
	  By=U0[i1]-U1[i1];                                   \
	  Cx=V0[i0]-U0[i0];                                   \
	  Cy=V0[i1]-U0[i1];                                   \
	  f=Ay*Bx-Ax*By;                                      \
	  d=By*Cx-Bx*Cy;                                      \
	  if((f>0 && d>=0 && d<=f) || (f<0 && d<=0 && d>=f))  \
	  {                                                   \
		e=Ax*Cy-Ay*Cx;                                    \
		if(f>0)                                           \
		{                                                 \
		  if(e>=0 && e<=f) return 1;                      \
		}                                                 \
		else                                              \
		{                                                 \
		  if(e<=0 && e>=f) return 1;                      \
		}                                                 \
	  }                                

	#define EDGE_AGAINST_TRI_EDGES(V0,V1,U0,U1,U2) \
	{                                              \
	  float Ax,Ay,Bx,By,Cx,Cy,e,d,f;               \
	  Ax=V1[i0]-V0[i0];                            \
	  Ay=V1[i1]-V0[i1];                            \
	  /* test edge U0,U1 against V0,V1 */          \
	  EDGE_EDGE_TEST(V0,U0,U1);                    \
	  /* test edge U1,U2 against V0,V1 */          \
	  EDGE_EDGE_TEST(V0,U1,U2);                    \
	  /* test edge U2,U1 against V0,V1 */          \
	  EDGE_EDGE_TEST(V0,U2,U0);                    \
	}

	#define POINT_IN_TRI(V0,U0,U1,U2)           \
	{                                           \
	  float a,b,c,d0,d1,d2;                     \
	  /* is T1 completly inside T2? */          \
	  /* check if V0 is inside tri(U0,U1,U2) */ \
	  a=U1[i1]-U0[i1];                          \
	  b=-(U1[i0]-U0[i0]);                       \
	  c=-a*U0[i0]-b*U0[i1];                     \
	  d0=a*V0[i0]+b*V0[i1]+c;                   \
												\
	  a=U2[i1]-U1[i1];                          \
	  b=-(U2[i0]-U1[i0]);                       \
	  c=-a*U1[i0]-b*U1[i1];                     \
	  d1=a*V0[i0]+b*V0[i1]+c;                   \
												\
	  a=U0[i1]-U2[i1];                          \
	  b=-(U0[i0]-U2[i0]);                       \
	  c=-a*U2[i0]-b*U2[i1];                     \
	  d2=a*V0[i0]+b*V0[i1]+c;                   \
	  if(d0*d1>0.0)                             \
	  {                                         \
		if(d0*d2>0.0) return 1;                 \
	  }                                         \
	}

	#define ISECT(VV0,VV1,VV2,D0,D1,D2,isect0,isect1) \
				  isect0=VV0+(VV1-VV0)*D0/(D0-D1);    \
				  isect1=VV0+(VV2-VV0)*D0/(D0-D2);

	static int coplanar_tri_tri(glm::vec3 N, glm::vec3 V0, glm::vec3 V1, glm::vec3 V2, glm::vec3 U0, glm::vec3 U1, glm::vec3 U2)
	{
	   float A[3];
	   short i0,i1;
	   /* first project onto an axis-aligned plane, that maximizes the area */
	   /* of the triangles, compute indices: i0,i1. */
	   A[0]=fabs(N[0]);
	   A[1]=fabs(N[1]);
	   A[2]=fabs(N[2]);
	   if(A[0]>A[1])
	   {
		  if(A[0]>A[2])  
		  {
			  i0=1;      /* A[0] is greatest */
			  i1=2;
		  }
		  else
		  {
			  i0=0;      /* A[2] is greatest */
			  i1=1;
		  }
	   }
	   else   /* A[0]<=A[1] */
	   {
		  if(A[2]>A[1])
		  {
			  i0=0;      /* A[2] is greatest */
			  i1=1;                                           
		  }
		  else
		  {
			  i0=0;      /* A[1] is greatest */
			  i1=2;
		  }
		}               
				
		/* test all edges of triangle 1 against the edges of triangle 2 */
		EDGE_AGAINST_TRI_EDGES(V0,V1,U0,U1,U2);
		EDGE_AGAINST_TRI_EDGES(V1,V2,U0,U1,U2);
		EDGE_AGAINST_TRI_EDGES(V2,V0,U0,U1,U2);
				
		/* finally, test if tri1 is totally contained in tri2 or vice versa */
		POINT_IN_TRI(V0,U0,U1,U2);
		POINT_IN_TRI(U0,V0,V1,V2);

		return 0;
	}

	#define COMPUTE_INTERVALS(VV0,VV1,VV2,D0,D1,D2,D0D1,D0D2,isect0,isect1) \
	if(D0D1>0.0f)                                         \
	{                                                     \
		/* here we know that D0D2<=0.0 */                   \
		/* that is D0, D1 are on the same side, D2 on the other or on the plane */ \
		ISECT(VV2,VV0,VV1,D2,D0,D1,isect0,isect1);          \
	}                                                     \
	else if(D0D2>0.0f)                                    \
	{                                                     \
		/* here we know that d0d1<=0.0 */                   \
		ISECT(VV1,VV0,VV2,D1,D0,D2,isect0,isect1);          \
	}                                                     \
	else if(D1*D2>0.0f || D0!=0.0f)                       \
	{                                                     \
		/* here we know that d0d1<=0.0 or that D0!=0.0 */   \
		ISECT(VV0,VV1,VV2,D0,D1,D2,isect0,isect1);          \
	}                                                     \
	else if(D1!=0.0f)                                     \
	{                                                     \
		ISECT(VV1,VV0,VV2,D1,D0,D2,isect0,isect1);          \
	}                                                     \
	else if(D2!=0.0f)                                     \
	{                                                     \
		ISECT(VV2,VV0,VV1,D2,D0,D1,isect0,isect1);          \
	}                                                     \
	else                                                  \
	{                                                     \
		/* triangles are coplanar */                        \
		return coplanar_tri_tri(n1,(a->points)[0], (a->points)[1], (a->points)[2], (b->points)[0], (b->points)[1], (b->points)[2]);      \
	}

};