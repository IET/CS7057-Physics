#include "BoundingVolume.h"


BoundingVolume::BoundingVolume(void)
{
	isColliding = false;
}

void BoundingVolume::Update(glm::mat4* modelMat)
{
	worldMaxBounds = (*modelMat) * maxBounds;
	worldMinBounds = (*modelMat) * minBounds;
	
	this->position = (*modelMat)[3];
	this->modelMatrix = modelMat;
}

glm::vec4 BoundingVolume::GetWorldMaxBounds()
{
	return this->worldMaxBounds;
}

glm::vec4 BoundingVolume::GetWorldMinBounds()
{
	return this->worldMinBounds;
}

glm::vec4 BoundingVolume::GetMaxBounds()
{
	return this->maxBounds;
}

glm::vec4 BoundingVolume::GetMinBounds()
{
	return this->minBounds;
}

glm::vec4 BoundingVolume::GetPosition()
{
	return this->position;
}

BoundingVolume::~BoundingVolume(void)
{
}
