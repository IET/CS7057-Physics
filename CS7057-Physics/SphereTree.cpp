#include "SphereTree.h"


SphereTree::SphereTree(void)
{
}

void SphereTree::Draw(glm::mat4 &v, glm::mat4 &p, int level)
{
	if (level > depth)
		level = depth;
	if (level < 0)
		level = 0;

	DrawNodes(rootNode, v, p, level);
}
/*
void SphereTree::ConnectNeighbours(Node* node)
{
	for (int i = 0; i < node->children.size() - 1; i++)
	{
		node->children[i].neighbour = &node->children[i+1];

		if (node->children[i].children.size() == 0)
			continue;

		for (int j = 0; j < node->children[i].children.size() - 1; j++)
		{
			node->children[i].children[j].neighbour = &node->children[i].children[j+1];
		}

		if (node->children[i + 1].children.size() > 0)
			node->children[i].children[node->children[i].children.size() - 1].neighbour = &node->children[i + 1].children[0];

		if (node->children[i].children.size() > 0)
		{
			ConnectNeighbours(&node->children[i]);
		}
	}
}
*/

void SphereTree::ConnectNeighbours()
{
	for (int i = 0; i < depth + 1; i++)
	{
		std::vector<BVHNode*> levelNeighbours;
		FindNeighbours(rootNode, levelNeighbours, i);
		
		for (int j = 0; j < levelNeighbours.size() - 1; j++)
		{
			levelNeighbours[j]->neighbour = levelNeighbours[j + 1];
		}
		levelNeighbours[levelNeighbours.size() - 1] = nullptr;
	}
}

void SphereTree::FindNeighbours(BVHNode *node, std::vector<BVHNode*> &connections, int level)
{
	if (level == 0)
	{
		connections.push_back(node);		
		return;
	}
	else
	{
		level--;
		for (int i = 0; i < node->children.size(); i++)
		{
			FindNeighbours(node->children[i], connections, level);
		}
	}
}


/*
void SphereTree::DrawNodes(Node* node, glm::mat4 &v, glm::mat4 &p, int level)
{
	if (level == 0)
	{
		node->boundingSphere.Draw(v, p, glm::vec3(0.0f, 0.0f, 1.0f));
		return;
	}
	else
	{
		level--;
		for (int i = 0; i < node->children.size(); i++)
		{
			DrawNodes(&node->children[i], v, p, level);
		}
	}
}
*/


void SphereTree::DrawNodes(BVHNode* node, glm::mat4 &v, glm::mat4 &p, int level)
{
	BVHNode *drawNode = rootNode;

	for (int i = 0; i < level; i++)
	{
		//Find the level to start drawing at
		drawNode = drawNode->children[0];
	}

	while (drawNode != nullptr)
	{
		if (drawNode->triangles.size() > 0)
			drawNode->boundingSphere.Draw(v, p, glm::vec3(0.0f, 0.0f, 1.0f));
	
		drawNode = drawNode->neighbour;
	}
}
SphereTree::~SphereTree(void)
{
}
