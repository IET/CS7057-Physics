#include "IntersectionTest.h"



bool IntersectionTest::TestIntersection(BoundingSphere *s1, BoundingSphere *s2)
{
	if (glm::length(s1->position - s2->position) < (s1->radius + s2->radius))
		return true;

	return false;
}

bool IntersectionTest::TestIntersection(BoundingSphere *s1, BoundingSphere *s2, glm::mat4 &m1, glm::mat4 &m2)
{
	s1->Update(&m1);
	s2->Update(&m1);

	if (glm::length(s1->position - s2->position) < (s1->radius + s2->radius))
		return true;

	return false;
}

//TODO - Implement Sphere-Sphere sweep test
bool IntersectionTest::TestIntersection(BoundingSphere *s1, BoundingSphere *s2, float deltaTime)
{
	return false;
}


bool IntersectionTest::TestIntersection(OBB* a, OBB *b)
{
	glm::vec4 v = a->GetPosition() - b->GetPosition();

	glm::vec4 T = *a->modelMatrix * v;


	//Rotation matrix to transform other OBB into this one's frame
	glm::mat3 R;

	//ra = projection of boxA onto axis
	//t = projection of vector between box centres onto axis
	float ra, rb, t;

	//Constructing orientation matrix to trnsform B into A's local coordinate system
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			glm::mat4 *other = b->modelMatrix;
			R[i][j] = glm::dot((*a->modelMatrix)[i], (*other)[j]);
		}
	}

	//Checking against A's basis vectors
	for (int i =0; i < 3; i++)
	{
		ra = a->GetWorldMaxBounds()[i];
		//B's extents along A's basis vectors
		rb = b->GetWorldMaxBounds()[0] * fabs(R[i][0]) + b->GetWorldMaxBounds()[0] * fabs(R[i][1]) + b->GetWorldMaxBounds()[2] * fabs(R[i][2]);
	
		t = fabs(T[i]);

		if (ra + rb < t)
			return false;
	}

	//Checking against B's basis vectors
	for (int i =0; i < 3; i++)
	{
		//A's extents along B's basis vectors
		ra = a->GetWorldMaxBounds()[0] * fabs(R[0][i]) + a->GetWorldMaxBounds()[1] * fabs(R[1][i]) + a->GetWorldMaxBounds()[2] * fabs(R[2][i]);
		rb = b->GetWorldMaxBounds()[i];
	
		t = fabs(glm::dot(glm::vec3(T), R[0]));

		if (ra + rb < t)
			return false;
	}

	//Checking edge directions
	//Ax X Bx
	ra = a->GetWorldMaxBounds().y * fabs(R[2][0]) + a->GetWorldMaxBounds().z * fabs(R[1][0]);
	rb = b->GetWorldMaxBounds().x * fabs(R[0][2]) + b->GetWorldMaxBounds().z * fabs(R[0][1]);

	t = fabs((T.z * R[1][0]) - (T.y * R[2][0]));

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;
	
	//Ax X By
	ra = a->GetWorldMaxBounds().y * fabs(R[2][1]) + a->GetWorldMaxBounds().z * fabs(R[1][1]);
	rb = b->GetWorldMaxBounds().x * fabs(R[0][2]) + b->GetWorldMaxBounds().z * fabs(R[0][0]);

	t = fabs((T.z * R[1][1]) - (T.x * R[2][1]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ax X Bz
	ra = a->GetWorldMaxBounds().y * fabs(R[2][2]) + a->GetWorldMaxBounds().z * fabs(R[1][2]);
	rb = b->GetWorldMaxBounds().x * fabs(R[0][1]) + b->GetWorldMaxBounds().y * fabs(R[0][0]);

	t = fabs((T.z * R[1][2]) - (T.y * R[2][2]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ay X Bx
	ra = a->GetWorldMaxBounds().x * fabs(R[2][0]) + a->GetWorldMaxBounds().z * fabs(R[0][0]);
	rb = b->GetWorldMaxBounds().y * fabs(R[1][2]) + b->GetWorldMaxBounds().z * fabs(R[1][1]);

	t = fabs((T.x * R[2][0]) - (T.z * R[0][0]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ay X By
	ra = a->GetWorldMaxBounds().x * fabs(R[2][1]) + a->GetWorldMaxBounds().z * fabs(R[0][1]);
	rb = b->GetWorldMaxBounds().x * fabs(R[1][2]) + b->GetWorldMaxBounds().z * fabs(R[1][0]);

	t = fabs((T.x * R[2][1]) - (T.z * R[0][1]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ay X Bz
	ra = a->GetWorldMaxBounds().x * fabs(R[2][2]) + a->GetWorldMaxBounds().z * fabs(R[0][2]);
	rb = b->GetWorldMaxBounds().x * fabs(R[1][1]) + b->GetWorldMaxBounds().y * fabs(R[1][0]);

	t = fabs((T.x * R[2][2]) - (T.z * R[0][2]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;
	
	//Az X Bx
	ra = a->GetWorldMaxBounds().x * fabs(R[1][0]) + a->GetWorldMaxBounds().x * fabs(R[0][0]);
	rb = b->GetWorldMaxBounds().y * fabs(R[2][2]) + b->GetWorldMaxBounds().z * fabs(R[2][1]);

	t = fabs((T.y * R[0][0]) - (T.x * R[1][0]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Az X By
	ra = a->GetWorldMaxBounds().x * fabs(R[1][1]) + a->GetWorldMaxBounds().y * fabs(R[0][1]);
	rb = b->GetWorldMaxBounds().x * fabs(R[2][2]) + b->GetWorldMaxBounds().z * fabs(R[2][0]);

	t = fabs((T.y * R[0][1]) - (T.x * R[1][1]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Az X Bz
	ra = a->GetWorldMaxBounds().x * fabs(R[1][2]) + a->GetWorldMaxBounds().y * fabs(R[0][2]);
	rb = b->GetWorldMaxBounds().x * fabs(R[2][1]) + b->GetWorldMaxBounds().y * fabs(R[2][0]);

	t = fabs((T.y * R[0][2]) - (T.z * R[1][2]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//If you've gotten this far, you've earned this
	return true;;
}

bool IntersectionTest::TestIntersection(OBB* a, BoundingSphere *b)
{
	glm::vec3 ab = glm::vec3(b->position) - glm::vec3(a->position);

	//TODO - Finish this
	return false;
}

bool IntersectionTest::TestIntersection(BoundingSphere *b, OBB* a)
{
	return IntersectionTest::TestIntersection(a, b);
}

bool IntersectionTest::TestIntersection(BoundingSphere* s, Triangle *t)
{
	//Implementation based on the description here: http://realtimecollisiondetection.net/blog/?p=103

	//Triangle ponts
	glm::vec3 p0, p1, p2;

	//define triangle vertices relative to sphere center
	p0 = t->points[0] - glm::vec3(s->position);
	p1 = t->points[1] - glm::vec3(s->position);
	p2 = t->points[2] - glm::vec3(s->position);

	glm::vec3 V = glm::cross(p1 - p0, p2 - p0);
	float d = glm::dot(p0, V);
	float e = glm::dot(V, V);

	if ( (d * d) > (s->radius * s->radius) * e )
		return false;

	//Test if sphere is outside of triangle vertices
	float p0p0 = dot(p0, p0);
	float p0p1 = dot(p0, p1);
	float p0p2 = dot(p0, p2);
		
	float p1p1 = dot(p1, p1);
	float p1p2 = dot(p1, p2);
		
	float p2p2 = dot(p2, p2);
		
	float rr = s->radius * s->radius;

	if((p0p0 > rr) & (p0p1 > p0p0) & (p0p2 > p0p0))
		return false;

	if ((p1p1 > rr) & (p0p1 > p1p1) & (p1p2 > p1p1))
		return false;

	if ( (p2p2 > rr) & (p0p2 > p2p2) & (p1p2 > p2p2))
		return false;
		
	//Test if sphere lies outside triangle edges
	glm::vec3 e0 = p1 - p0;
	glm::vec3 e1 = p2 - p1;
	glm::vec3 e2 = p0 - p2;

	float d0 = p0p1 - p0p0;
	float d1 = p1p2 - p1p1;
	float d2 = p0p2 - p2p2;
		
	float l0 = glm::dot(e0, e0);
	float l1 = glm::dot(e1, e1);
	float l2 = glm::dot(e2, e2);
		
	glm::vec3 Q0 = (p0 * l0) - (d0 * e0);
	glm::vec3 Q1 = (p1 * l1) - (d1 * e1);
	glm::vec3 Q2 = (p2 * l2) - (d2 * e2);

	glm::vec3 QA = (p0 * l1) - Q1;
	glm::vec3 QB = (p1 * l2) - Q2;;
	glm::vec3 QC = (p2 * l0) - Q0;

	if ((glm::dot(Q0, Q0) > (rr * l0 * l0)) && (glm::dot(Q0, QC) > 0))
		return false;

	if ((glm::dot(Q1, Q1) > (rr * l1 * l1)) && (glm::dot(Q1, QA) > 0))
		return false;

	if ((glm::dot(Q2, Q2) > (rr * l2 * l2)) && (glm::dot(Q2, QB) > 0))
		return false;

	return true;
}

//Triangle - Triangle Intersection Test
bool IntersectionTest::TestIntersection(Triangle* a, Triangle *b)
{
	// Courtesy of http://web.archive.org/web/19990203013328/http://www.acm.org/jgt/papers/Moller97/tritri.html
	float epsilon = 0.000001f;

	//Compute plane equation of a
	glm::vec3 e1 = (a->points)[1] - (a->points)[0];
	glm::vec3 e2 = (a->points)[2] - (a->points)[2];
	glm::vec3 n1 = glm::cross(e1, e2);
	float d1 = -glm::dot(n1, (a->points)[0]);
	//Plane equation 1: N1.X+d1=0
	
	//Insert points of b into plane eqation of a to compute signed distances to plane
	float d0b = glm::dot(n1, (b->points)[0]) + d1;
	float d1b = glm::dot(n1, (b->points)[1]) + d1;
	float d2b = glm::dot(n1, (b->points)[2]) + d1;

	//Coplanarity robustness test
	if ( fabs(d0b) < epsilon)
		d0b = 0.0f;
	if ( fabs(d1b) < epsilon)
		d1b = 0.0f;
	if ( fabs(d2b) < epsilon)
		d2b = 0.0f;
	
	float d0bd1b = d0b * d1b;
	float d0bd2b = d0b * d2b;
	
	//Same sign on all and not == 0, no intersection
	if (d0bd1b > 0.0f && d0bd2b > 0.0f) 
		return false;

	//Compute plane equation of b
	e1 = (a->points)[1] - (*a->points)[0];
	e2 = (a->points)[2] - (*a->points)[2];
	glm::vec3 n2 = glm::cross(e1, e2);
	float d2 = -glm::dot(n1, (a->points)[0]);

	//Insert points of a into plane eqation of b to compute signed distances to plane
	float d0a = glm::dot(n2, (a->points)[0]) + d2;
	float d1a = glm::dot(n2, (a->points)[1]) + d2;
	float d2a = glm::dot(n2, (a->points)[2]) + d2;

	//Coplanarity robustness test
	if ( fabs(d0a) < epsilon)
		d0a = 0.0f;
	if ( fabs(d1a) < epsilon)
		d1a = 0.0f;
	if ( fabs(d2a) < epsilon)
		d2a = 0.0f;
	
	float d0ad1a = d0a * d1a;
	float d0ad2a = d0a * d2a;
	
	//Same sign on all and not == 0, no intersection
	if (d0ad1a > 0.0f && d0ad2a > 0.0f) 
		return false;
	
	//Compute direction of intersection line
	glm::vec3 d = glm::cross(n1, n2);

	//Compute and index into the largest element of d
	float max = fabs(d[0]);
	int index = 0;
	float D1 = fabs(d[1]);
	float D2 = fabs(d[2]);
	
	if (D1 > max)
	{
		max = D1;
		index = 1;
	}
	if (D2 > max)
	{
		max = D2;
		index = 2;
	}

	//Simplified projection onto L
	glm::vec3 pa;
	pa[0] = (a->points)[0][index];
	pa[1] = (a->points)[1][index];
	pa[2] = (a->points)[2][index];

	glm::vec3 pb;
	pb[0] = (b->points)[0][index];
	pb[1] = (b->points)[1][index];
	pb[2] = (b->points)[2][index];
	
	//TODO - Compute intervals
	
	float interval1[2];
	float interval2[2];

	COMPUTE_INTERVALS(pa[0], pa[1], pa[2], d0a, d1a, d2a, d0ad1a, d0ad2a, interval1[0], interval1[1]);
	COMPUTE_INTERVALS(pb[0], pb[1], pb[2], d0b, d1b, d2b, d0bd1b, d0bd2b, interval2[0], interval2[1]);

	if (interval1[0] < interval1[1])
	{
		float temp = interval1[0];
		interval1[0] = interval1[1];
		interval1[1] = temp;
	}

	if (interval2[0] < interval2[1])
	{
		float temp = interval2[0];
		interval2[0] = interval2[1];
		interval2[1] = temp;
	}


	if ( (interval1[1] < interval2[0]) || (interval2[1] < interval1[0]) )
		return false;

	//You've made it this far, you deserve it
	return true;
}

