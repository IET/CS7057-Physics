#include "Octree.h"
#include "IntersectionTest.h"

Octree::Octree(void)
{
}

Octree::Octree(std::vector<Triangle> &triangles, int inDepth)
{
	Init(triangles, inDepth);
}

void Octree::Init(std::vector<Triangle> &triangles, int inDepth)
{
	this->depth = inDepth;
	
	std::vector<Triangle*> rootTriangles;

	for (int i = 0; i < triangles.size(); i++)
		rootTriangles.push_back(&triangles[i]);

	rootNode = new BVHNode(rootTriangles);
	BVHNode* currentNode = rootNode;
	
	GenerateSpheres(rootNode, depth);
	ConnectNeighbours();
}

void Octree::GenerateSpheres(BVHNode*& node, int depth)
{
	if (depth == 0 || node->triangles.size() == 0)
		return;

	node->children.resize(8);
	
	//Generate child nodes
	glm::vec3 position;
	
	float radius;
	radius = glm::distance(node->boundingSphere.obbMinBounds, node->boundingSphere.obbMaxBounds)/4.0f;
	
	for (int i = 0 ; i < node->children.size(); i++)
	{
		node->children[i] = new BVHNode;
	}

	//Top
	//Left
	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(-1.0f, 1.0f, 1.0f)));
	node->children[0]->boundingSphere = BoundingSphere(node->boundingSphere.center, node->boundingSphere.obbMaxBounds);

	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(-1.0f, -1.0f, 1.0f)));
	node->children[1]->boundingSphere = BoundingSphere(node->boundingSphere.center, glm::vec3(node->boundingSphere.obbMaxBounds.x, node->boundingSphere.obbMaxBounds.y, node->boundingSphere.obbMinBounds.z));
	
	//Right
	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(1.0f, 1.0f, 1.0f)));
	node->children[2]->boundingSphere = BoundingSphere(node->boundingSphere.center, glm::vec3(node->boundingSphere.obbMinBounds.x, node->boundingSphere.obbMaxBounds.y, node->boundingSphere.obbMaxBounds.z));

	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(1.0f, -1.0f, 1.0f)));
	node->children[3]->boundingSphere = BoundingSphere(node->boundingSphere.center, glm::vec3(node->boundingSphere.obbMinBounds.x, node->boundingSphere.obbMaxBounds.y, node->boundingSphere.obbMinBounds.z));

	//Bottom
	//Left
	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(-1.0f, 1.0f, -1.0f)));
	node->children[4]->boundingSphere = BoundingSphere(glm::vec3(node->boundingSphere.obbMinBounds.x, node->boundingSphere.obbMinBounds.y, node->boundingSphere.obbMinBounds.z), node->boundingSphere.center);

	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(-1.0f, -1.0f, -1.0f)));
	node->children[5]->boundingSphere = BoundingSphere(glm::vec3(node->boundingSphere.obbMaxBounds.x, node->boundingSphere.obbMinBounds.y, node->boundingSphere.obbMinBounds.z), node->boundingSphere.center);

	//Right
	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(1.0f, 1.0f, -1.0f)));
	node->children[6]->boundingSphere = BoundingSphere(glm::vec3(node->boundingSphere.obbMinBounds.x, node->boundingSphere.obbMinBounds.y, node->boundingSphere.obbMaxBounds.z), node->boundingSphere.center);

	position = glm::vec3(node->boundingSphere.center) + (radius * glm::normalize(glm::vec3(1.0f, -1.0f, -1.0f)));
	node->children[7]->boundingSphere = BoundingSphere(glm::vec3(node->boundingSphere.obbMaxBounds.x, node->boundingSphere.obbMinBounds.y, node->boundingSphere.obbMaxBounds.z), node->boundingSphere.center);

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < node->triangles.size(); j++)
		{
			if (IntersectionTest::TestIntersection(&node->children[i]->boundingSphere, node->triangles[j]))
			{
				node->children[i]->triangles.push_back(node->triangles[j]);
			}
		}
	}

	//Cull children with no triangles in them (not sure this is strictly an octree anymore)
	for (int i = 0; i < node->children.size(); i++)
	{
		if (node->children[i]->triangles.size() == 0)
		{
			delete node->children[i];
			node->children.erase(node->children.begin() + i);
			//Decrement so as not to skip the next node after deletion 
			i--;
		}
	}

	for (int i = 0; i < node->children.size(); i++)
	{
		if (node->children[i]->triangles.size() > 0)
		{
			GenerateSpheres(node->children[i], depth-1);
		}
	}
}

void Octree::Update(glm::mat4 *modelMatrix)
{
	UpdateNode(rootNode, modelMatrix);
}

void Octree::UpdateNode(BVHNode *node, glm::mat4 *modelMatrix)
{
	node->boundingSphere.Update(modelMatrix);
	node->boundingSphere.isColliding = false;

	for (int i=0; i < node->children.size(); i++)
	{
		UpdateNode(node->children[i], modelMatrix);
	}
}

Octree::~Octree(void)
{
}
