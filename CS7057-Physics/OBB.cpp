#include "OBB.h"
#include <math.h>

OBB::OBB()
{

}

OBB::OBB(glm::vec4 minBounds, glm::vec4 maxBounds)
{
	this->minBounds = minBounds;
	this->maxBounds = maxBounds;

	this ->shader = ShaderManager::loadShader("bv_debug");
	/* Create our list of vertex indices */

	//Such a minor optimisation but hey! Why define these for each OBB??
	GLuint indices[] =
	{
		// Front Face
		0, 1,
		1, 2, 
		2, 3,
		3, 0,

		//Right Face
		0,4,
		4,5,
		5,1,

		//Back Face
		4,7,
		7,6,
		6,5,

		//Left Face
		7,3,
		2,6

	};

	GLfloat vertices[] = {
		maxBounds.x, maxBounds.y, maxBounds.z,
		maxBounds.x, minBounds.y, maxBounds.z,
		minBounds.x, minBounds.y, maxBounds.z,
		minBounds.x, maxBounds.y, maxBounds.z,

		maxBounds.x, maxBounds.y, minBounds.z,
		maxBounds.x, minBounds.y, minBounds.z,
		minBounds.x, minBounds.y, minBounds.z,
		minBounds.x, maxBounds.y, minBounds.z,
	};

	/* Get GL to allocate space for our array buffers */
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	/* Tell GL we're now working on our Vertex Array Object */
	glBindVertexArray(this->VAO);
	
	/* Give GL our vertices */
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	/* Give GL our indices */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	/* Tell GL we don't need to work on our buffers any more */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

bool OBB::TestCollision(OBB* obb)
{
	//Based on code from here
	//http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?page=5

	//Need 3 face directions for A
	// + 3 face directions for y 
	// + 3x3 edge directions 
	//in which you need to search for a separating axis.

	//Gamasutra Notation
	//a	= extents
	//Pa = position
	//A	 = orthonormal basis


	//TODO - finish implementing this later.
	//Distance between objects in world space
	glm::vec4 v = this->GetPosition() - obb->GetPosition();

	glm::vec4 T = (*modelMatrix) * v;


	//Rotation matrix to transform other OBB into this one's frame
	glm::mat3 R;

	//ra = projection of boxA onto axis
	//t = projection of vector between box centres onto axis
	float ra, rb, t;
	glm::vec3 eA = glm::vec3(maxBounds - minBounds);
	glm::vec3 eB = glm::vec3(obb->maxBounds - obb->minBounds);

	//isColliding = false;

	//Constructing orientation matrix to trnsform B into A's local coordinate system
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			glm::mat4 *other = obb->modelMatrix;
			R[i][j] = glm::dot(glm::normalize(glm::vec3((*modelMatrix)[i])), glm::normalize(glm::vec3((*other)[j])));
		}
	}

	//Checking against A's basis vectors
	for (int i =0; i < 3; i++)
	{
		ra = eA[i];
		//B's extents along A's basis vectors
		rb = eB.x * fabs(R[i][0]) + eB.y * fabs(R[i][1]) + eB.z * fabs(R[i][2]);
	
		t = fabs(T[i]);

		if (ra + rb < t)
			return false;
	}

	//Checking against B's basis vectors
	for (int i =0; i < 3; i++)
	{
		//A's extents along B's basis vectors
		ra = eA.x * fabs(R[0][i]) + eA.y * fabs(R[1][i]) + eA.z * fabs(R[2][i]);
		rb = eB[i];
	
		t = fabs(glm::dot(glm::vec3(T), R[0]));

		if (ra + rb < t)
			return false;
	}

	isColliding = true;
	return true;
/*
	//Checking edge directions
	//Ax X Bx
	ra = this->maxBounds.y * fabs(R[2][0]) + this->maxBounds.z * fabs(R[1][0]);
	rb = obb->maxBounds.y * fabs(R[0][2]) + obb->maxBounds.z * fabs(R[0][1]);

	t = fabs((T.z * R[1][0]) - (T.y * R[2][0]));

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;
	
	//Ax X By
	ra = this->maxBounds.y * fabs(R[2][1]) + this->maxBounds.z * fabs(R[1][1]);
	rb = obb->maxBounds.x * fabs(R[0][2]) + obb->maxBounds.z * fabs(R[0][0]);

	t = fabs((T.z * R[1][1]) - (T.y * R[2][1]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ax X Bz
	ra = this->maxBounds.y * fabs(R[2][2]) + this->maxBounds.z * fabs(R[1][2]);
	rb = obb->maxBounds.x * fabs(R[0][1]) + obb->maxBounds.y * fabs(R[0][0]);

	t = fabs((T.z * R[1][2]) - (T.y * R[2][2]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ay X Bx
	ra = this->maxBounds.x * fabs(R[2][0]) + this->maxBounds.z * fabs(R[0][0]);
	rb = obb->maxBounds.y * fabs(R[1][2]) + obb->maxBounds.z * fabs(R[1][1]);

	t = fabs((T.x * R[2][0]) - (T.z * R[0][0]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ay X By
	ra = this->maxBounds.x * fabs(R[2][1]) + this->maxBounds.z * fabs(R[0][1]);
	rb = obb->maxBounds.x * fabs(R[1][2]) + obb->maxBounds.z * fabs(R[1][0]);

	t = fabs((T.x * R[2][1]) - (T.z * R[0][1]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Ay X Bz
	ra = this->maxBounds.x * fabs(R[2][2]) + this->maxBounds.z * fabs(R[0][2]);
	rb = obb->maxBounds.x * fabs(R[1][1]) + obb->maxBounds.y * fabs(R[1][0]);

	t = fabs((T.x * R[2][2]) - (T.z * R[0][2]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Az X Bx
	ra = this->maxBounds.x * fabs(R[1][0]) + this->maxBounds.x * fabs(R[0][0]);
	rb = obb->maxBounds.y * fabs(R[2][2]) + obb->maxBounds.z * fabs(R[2][1]);

	t = fabs((T.y * R[0][0]) - (T.x * R[1][0]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Az X By
	ra = this->maxBounds.x * fabs(R[1][1]) + this->maxBounds.y * fabs(R[0][1]);
	rb = obb->maxBounds.x * fabs(R[2][2]) + obb->maxBounds.z * fabs(R[2][0]);

	t = fabs((T.y * R[0][1]) - (T.x * R[1][1]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	//Az X Bz
	ra = this->maxBounds.x * fabs(R[1][2]) + this->maxBounds.y * fabs(R[0][2]);
	rb = obb->maxBounds.x * fabs(R[2][1]) + obb->maxBounds.y * fabs(R[2][0]);

	t = fabs((T.y * R[0][2]) - (T.z * R[1][2]));	

	//There is no overlap on this axis, exit
	if (t > (ra + rb))
		return false;

	isColliding = true;
	//If you've gotten this far, you've earned this
	return true;
*/
}

/*
void OBB::Update(glm::mat4 *modelMat)
{
	minBounds = *modelMat * minBounds;
	maxBounds = *modelMat * maxBounds;

	modelMatrix = modelMat;
}
*/

void OBB::Draw(glm::mat4 v, glm::mat4 p)
{
	this->shader->enableShader();
	this->shader->setUniformMatrix4fv("modelMat", *(this->modelMatrix));
	this->shader->setUniformMatrix4fv("viewMat",		v);
	this->shader->setUniformMatrix4fv("projectionMat",  p);

	if (isColliding)
		this->shader->setUniformVector4fv("col", glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	else
		this->shader->setUniformVector4fv("col", glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));

	glBindVertexArray(this->VAO);
	glDrawElements(GL_LINES, 24, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}


OBB::~OBB(void)
{
}
