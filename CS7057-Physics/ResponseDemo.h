#pragma once
#include "demobase.h"

class ResponseDemo :
	public DemoBase
{
private:
	Shader *texShader;
	Model rectangle;
	Model planeModel;
	
	vector<BVHNode> collidingNodes;

public:
	ResponseDemo(void);

	void Init();

	void Update(float dt);

	void Draw(glm::mat4 v, glm::mat4 p);
	
	void HandleInput(bool *keys);

	void Cleanup();

	~ResponseDemo(void);
};

