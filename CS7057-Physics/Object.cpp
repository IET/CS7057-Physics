#include "Object.h"

std::vector<Object*> Object::Objects;

Object::Object(){
	//this->Objects.push_back(this);
	this->position = glm::vec3(0);
	this->rotation = glm::vec3(0);
	this->scaleFactor = glm::vec3(0);

	this->modelMatrix = glm::mat4();
}
/*
Object::Object(const char* filepath){
	this->model = new Model(filepath);
	
	this->position		= glm::vec3(0);
	this->rotation		= glm::vec3(0);
	this->scaleFactor	= glm::vec3(0);
	
	this->modelMatrix	= glm::mat4();
	
	this->Objects.push_back(this);
}
*/
Object::Object(Model m){
	this->model = m;
	this->Objects.push_back(this);

	this->position		= glm::vec3(0);
	this->rotation		= glm::vec3(1);
	this->scaleFactor	= glm::vec3(0);

	this->scaleFactor = glm::vec3(1);
	this->modelMatrix = glm::mat4();
}

Object::Object(Model m, glm::vec3 location)
{
	this->model = m;
	this->Objects.push_back(this);

	this->position		= location;
	this->rotation		= glm::vec3(1);
	this->scaleFactor	= glm::vec3(0);

	this->scaleFactor = glm::vec3(1);
	this->modelMatrix = glm::mat4();
}


glm::vec3 Object::GetPosition()
{
	return this->position;
}

glm::vec3 Object::GetRotation()
{
	return this->rotation;
}

glm::mat4 Object::GetModelMatrix()
{
	return this->modelMatrix;
}

glm::mat3 Object::GetOrientationMatrix()
{
	glm::mat3 m = glm::mat3(modelMatrix);
	
	//m[0] = glm::normalize(m[0]);
	//m[1] = glm::normalize(m[1]);
	//m[2] = glm::normalize(m[2]);

	return m;
}


void Object::Draw(glm::mat4 view, glm::mat4 proj){
	
	this->model.getShader()->enableShader();
	this->model.getShader()->setUniformMatrix4fv("projectionMat", proj);
	this->model.getShader()->setUniformMatrix4fv("viewMat", view);

	if (this->texture)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		model.getShader()->setUniform1i("tex", 0);		//Bind texture to texture unit 0
	}

	this->model.Draw(modelMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	//TODO - I think shader should actually be stored here, not in model...
	this->model.getShader()->disableShader();
}

void Object::DrawObjects(glm::mat4 view, glm::mat4 proj)
{
	//TODO - Get this to work
	for (int i =0; i< Objects.size(); ++i)
	{
		throw(exception());
		//Objects[i]->Draw(proj, view);
	}
}

void Object::Translate(glm::vec3 translateBy)
{
	this->modelMatrix = glm::translate(this->modelMatrix, translateBy);
}

void Object::Rotate(glm::vec3 rotateBy)
{
	this->modelMatrix = glm::rotate(this->modelMatrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	this->modelMatrix = glm::rotate(this->modelMatrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	this->modelMatrix = glm::rotate(this->modelMatrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
}

//non-uniform scaling not currently supported
void Object::Scale(glm::vec3 scaleIn)
{
	this->modelMatrix = glm::scale(this->modelMatrix, scaleIn);
}

void Object::SetScale (glm::vec3 newScale) {
	this->scaleFactor = newScale;
}


void Object::setTexture(GLint handle)
{
	this->texture = handle;
}

Object::~Object()
{
}
