#include "PhysObject.h"
#include <algorithm>

std::vector<PhysObject*> PhysObject::PhysObjects;

PhysObject::PhysObject()
	:Object()
{
	this->linearMomentum		= glm::vec3(0);

	this->inverseMass = 1.0f;
	this->isCollider = true;
	this->restitutionCoeffcient = 0.9f;

	PhysObjects.push_back(this);
}

PhysObject::PhysObject(Model m)
	:Object(m)
{
	this->linearMomentum		= glm::vec3(0);

	this->inverseMass = 1.0f;
	this->isCollider = true;
	this->restitutionCoeffcient = 0.9f;
	
	Init();

	PhysObjects.push_back(this);
}

PhysObject::PhysObject(Model m, glm::vec3 position)
	:Object(m, position)
{	
	Init();
	/*
	this->linearMomentum		= glm::vec3(0);

	this->mass = 1.0f;
	this->isCollider = true;
	this->restitutionCoeffcient = 0.9f;

	this->position = position;

	this->bb.minBounds = model.minBounds;
	this->bb.maxBounds = model.maxBounds;

	this->SetScale(glm::vec3(10.0f));

	this->bb.planeNormals[0] = glm::vec3(1, 0, 0);	
	this->bb.planeNormals[1] = glm::vec3(0, 1, 0);	
	this->bb.planeNormals[2] = glm::vec3(0, 0, 1);
	this->bb.planeNormals[3] = glm::vec3(-1, 0, 0);
	this->bb.planeNormals[4] = glm::vec3(0, -1, 0);
	this->bb.planeNormals[5] = glm::vec3(0, 0, -1);

	this->bb.contactPoints[0] = glm::vec3(this->position.x + (this->bb.maxBounds.x), 0, 0);	
	this->bb.contactPoints[1] = glm::vec3(0, this->position.y + (this->bb.maxBounds.y), 0);	
	this->bb.contactPoints[2] = glm::vec3(0, 0, this->position.z + (this->bb.maxBounds.z));
	this->bb.contactPoints[3] = glm::vec3(this->position.x - (this->bb.maxBounds.x), 0, 0);
	this->bb.contactPoints[4] = glm::vec3(0, this->position.y - (this->bb.maxBounds.y), 0);
	this->bb.contactPoints[5] = glm::vec3(0, 0, this->position.z - (this->bb.maxBounds.z));
	
	float height	= bb.maxBounds.y - bb.minBounds.y;
	float width		= bb.maxBounds.x - bb.minBounds.x;
	float depth		= bb.maxBounds.z - bb.minBounds.z;

	this->dragCoefficient = 0.9f;

	inertiaTensor = glm::mat3( (1.0f/12.0f) * mass * ((height * height) + (depth * depth)), 0.0f	, 0.0f,
								0.0f, (1.0f/12.0f) * mass * ((width * width) + (depth * depth)), 0.0f,
								0.0f, 0.0f, (1.0f/12.0f) * mass * ((width * width) + (height * height)));

	PhysObjects.push_back(this);
	*/
	PhysObjects.push_back(this);
}

PhysObject::PhysObject(Model m, glm::vec3 position, glm::vec3 rotation, glm::vec3 linearMomentum, glm::vec3 angularMomentum)
	:Object(m)
{	
	Init();
	this->linearMomentum		= linearMomentum;
	this->angularMomentum		= angularMomentum;

	//this->linearAcceleration	= glm::vec3(0);
	//this->angularAcceleration	= glm::vec3(0);

	this->inverseMass = 0.05f;
	this->isCollider = true;
	this->restitutionCoeffcient = 0.8f;

	this->position = position;

	this->bv = OBB(glm::vec4(model.minBounds * 1.01f, 1.0f), glm::vec4(model.maxBounds  * 1.01f, 1.0f));
	this->bs = BoundingSphere(&model);
	
	this->dragCoefficient = 0.5f;
	this->centreOfMass = glm::vec3(0);

	float height	= model.maxBounds.y - model.minBounds.y;
	float width		= model.maxBounds.x - model.minBounds.x;
	float depth		= model.maxBounds.z - model.minBounds.z;
	
	glm::mat3 inertiaTensor = (1.0f/12.0f) * (1.0f/inverseMass) * glm::mat3(	((height * height) + (depth * depth)), 0.0f	,		0.0f,
																				0.0f,	((width * width) + (depth * depth)),		0.0f,
																				0.0f,	0.0f,		((width * width) + (height * height)));
	inverseInertiaTensor = glm::inverse(inertiaTensor);

	this->SetScale(glm::vec3(10.0f));

	PhysObjects.push_back(this);
	this->Update(0.0f);
}

void PhysObject::ApplyForce(glm::vec3 (*f) (glm::vec3)) {
	glm::vec3 force = (*f)(this->position);

	this->netForce += force;

	this->netTorque += glm::cross(force, centreOfMass);
}

void PhysObject::ApplyForce(glm::vec3 (*f) (glm::vec3), glm::vec3 offset) {
	glm::vec3 force = (*f)(this->position);

	this->netForce += force;

	this->netTorque += glm::cross(force, centreOfMass + offset);
}

void PhysObject::ApplyForceLocal(glm::vec3 f, glm::vec3 point) 
{
	netForce += f;

	glm::vec3 worldspaceCenterOfMass = glm::vec3( modelMatrix * glm::vec4(centreOfMass, 1.0f));

	netTorque += glm::cross(f, point);
}

void PhysObject::ApplyForce(glm::vec3 f, glm::vec3 point) 
{
	netForce += f;

	glm::vec3 worldspaceCenterOfMass = glm::vec3( modelMatrix * glm::vec4(centreOfMass, 1.0f));

	netTorque += glm::cross(f, (point - worldspaceCenterOfMass) );
}

void PhysObject::Stop()
{
	linearMomentum = glm::vec3(0);
	angularMomentum = glm::vec3(0);
}

void PhysObject::Update(float timeStep) {

	//TODO - Fix drag. It was causing instability of rigidbody and making it teleport away as though it was sick of my shit...
	//Wasn't appreciated
	//Apply Drag
	//glm::vec3 drag = 0.5f * (-linearMomentum) * dragCoefficient;
	//this->ApplyForce(drag, glm::vec3(glm::vec3(0)));

	glm::mat3 orientation  = GetOrientationMatrix();
	position += (linearMomentum * inverseMass) * timeStep;

	linearMomentum	+= (netForce);
	glm::mat3 inertia = orientation * inverseInertiaTensor * glm::transpose(orientation);

	angularMomentum += inertia * netTorque;
	glm::vec3 angularDrag = 0.5f * (-angularMomentum) * (-angularMomentum) * dragCoefficient;
	glm::vec3 angularVelocity = (angularMomentum * inverseMass);

	glm::mat3 rotationUpdate = glm::mat3(	0.0f,				-angularVelocity.z, angularVelocity.y,
											angularVelocity.z,	0.0f,				-angularVelocity.x,
											-angularVelocity.y, angularVelocity.x,	0.0f				);

	//Compute Rotational Update
	glm::mat3 rotationMatrix = orientation + (timeStep * (rotationUpdate * orientation));

	//Normalise and Re-Orthogonalise Axes
	rotationMatrix[0] = glm::normalize(rotationMatrix[0]);
	rotationMatrix[1] = glm::cross(rotationMatrix[2], rotationMatrix[0]);
	rotationMatrix[1] = glm::normalize(rotationMatrix[1]);
	rotationMatrix[2] = glm::cross(rotationMatrix[0], rotationMatrix[1]);
	rotationMatrix[2] = glm::normalize(rotationMatrix[2]);
	
	//Apply Rotation
	modelMatrix[0].x = rotationMatrix[0].x;
	modelMatrix[0].y = rotationMatrix[0].y;
	modelMatrix[0].z = rotationMatrix[0].z;

	modelMatrix[1].x = rotationMatrix[1].x;
	modelMatrix[1].y = rotationMatrix[1].y;
	modelMatrix[1].z = rotationMatrix[1].z;

	modelMatrix[2].x = rotationMatrix[2].x;
	modelMatrix[2].y = rotationMatrix[2].y;
	modelMatrix[2].z = rotationMatrix[2].z;

	//Apply Translation
	modelMatrix[3] = glm::vec4(position, modelMatrix[3].w);

	this->netForce = glm::vec3(0);
	this->netTorque = glm::vec3(0);

	modelMatrix = glm::scale(modelMatrix, scaleFactor);

	bv.Update(&modelMatrix);
	bs.Update(&modelMatrix);
	bvh.Update(&modelMatrix);
}

void PhysObject::Translate(glm::vec3 t) {
	this->modelMatrix = glm::translate(modelMatrix, t);
}

void PhysObject::SetScale (glm::vec3 newScale) {
	//this->bb.minBounds /= scaleFactor;	
	//this->bb.maxBounds /= scaleFactor;
	
	float height	= (model.maxBounds.y - model.minBounds.y) * newScale.y;
	float width		= (model.maxBounds.x - model.minBounds.x) * newScale.x;
	float depth		= (model.maxBounds.z - model.minBounds.z) * newScale.z;

	glm::mat3 inertiaTensor =	glm::mat3(	((height * height) + (depth * depth)), 0.0f	,		0.0f,
											0.0f,	((width * width) + (depth * depth)),		0.0f,
											0.0f,	0.0f,		((width * width) + (height * height)));
	
	inertiaTensor *= ((1.0f/inverseMass)/12.0f);

	inverseInertiaTensor = glm::inverse(inertiaTensor);

	this->scaleFactor = newScale;
//	this->modelMatrix = glm::scale(modelMatrix, scaleFactor);
	
	//this->bb.minBounds *= scaleFactor;	
	//this->bb.maxBounds *= scaleFactor;
}

void PhysObject::DrawBVH(glm::mat4 v,glm::mat4 p, int level)
{
	//bvh.Update(&this->modelMatrix);
	bvh.Draw(v, p, level);
}

float PhysObject::GetRestitutionCoeffcient()
{
	return this->restitutionCoeffcient;
}

glm::mat3 PhysObject::GetInverseInertiaTensor()
{
	return this->inverseInertiaTensor;
}

glm::vec3 PhysObject::GetCentreOfMass()
{
	return glm::vec3( modelMatrix * glm::vec4(centreOfMass, 1.0f));
}

glm::vec3 PhysObject::GetVelocity()
{
	return linearMomentum * inverseMass;
}

glm::vec3 PhysObject::GetAngularVelocity()
{
	return angularMomentum * inverseMass;
}

void PhysObject::Init()
{
	bvh.Init(model.triangles, 4);
}

PhysObject::~PhysObject() 
{
//	if (std::find(PhysObjects.begin(), PhysObjects.end(), this) != PhysObjects.end())
//	{
		//remove from vector
//	}
//	PhysObjects
}