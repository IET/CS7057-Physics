#include "Physics.h"

std::vector<PhysObject> Physics::PhysicsObjects;

bool Physics::NarrowPhase(PhysObject *obj1, PhysObject *obj2, vector<BVHNode> &collidingNodes, float deltaTime)
{
	std::vector<BVHNode> colliders;
	Physics::TestCollision(obj1->bvh.rootNode, obj2->bvh.rootNode, colliders);
	
	if (colliders.size() == 0)
	{
		return false;
	}
	else
	{
		collidingNodes = colliders;
		if (deltaTime < 0.0f)
		{
			return true;
		}
		glm::vec3 point1, point2;
		float penetrationDepth = (colliders[1].boundingSphere.radius + colliders[0].boundingSphere.radius)
									- glm::length(colliders[1].boundingSphere.position - colliders[0].boundingSphere.position); 
	
		int i;
		for( i =0; i < colliders.size(); i++)
		{
			if (i%2 == 0)
			{
				point1 += glm::vec3(colliders[i].boundingSphere.position);
			}
			else
				point2 += glm::vec3(colliders[i].boundingSphere.position);

			if (i < colliders.size() - 1)
			{
				float dist = (colliders[i+1].boundingSphere.radius + colliders[i].boundingSphere.radius)
					- glm::length(colliders[i + 1].boundingSphere.position - colliders[i].boundingSphere.position);

				if (dist > penetrationDepth)
					penetrationDepth = dist;
			}
		}

		point1 /= (float)(i/2);	
		point2 /= (float)(i/2);

		if (penetrationDepth > 0)
		{
			glm::vec3 n = glm::normalize(point2 - point1);

			obj1->position -= (n * (penetrationDepth)) * 1.2f;
			obj2->position += (n * (penetrationDepth)) * 1.2f;
		}
	
		Physics::CollisionResponse(obj1, obj2, point1, point2);
	}

	return true;
}

void Physics::TestCollision(BVHNode *&node1, BVHNode *&node2, vector<BVHNode> &collidingNodes)
{
	if (!IntersectionTest::TestIntersection(&node1->boundingSphere, &node2->boundingSphere))
		return;
	else
	{
		if (node1->children.size() > 0 && node2->children.size() > 0)
		{
			for (int i = 0; i < node1->children.size(); i++)
			{
				for (int j = 0; j < node2->children.size(); j++)
				{
					if (IntersectionTest::TestIntersection(&node1->children[i]->boundingSphere, &node2->children[j]->boundingSphere))
					{
						Physics::TestCollision(node1->children[i], node2->children[j], collidingNodes);
					}
				}
			}
			return;
		}

		else if (node1->children.size() > 0 || node2->children.size() > 0)
		{
			return;
		}
	}

	//TODO - Cull the list of potential colliders
	//At the moment this can spawn >1000 colliding nodes because of duplicates
	//Probably good to split them by obj1 and obj2 too

	collidingNodes.push_back(*node1);
	collidingNodes.push_back(*node2);
}


bool Physics::CollisionResponse(PhysObject *obj1, PhysObject *obj2, glm::vec3 &point1, glm::vec3 &point2)
{
	float epsilon = obj1->GetRestitutionCoeffcient() * obj2->GetRestitutionCoeffcient();

	
	glm::vec3 c1 = point1;
	glm::vec3 c2 = point2;
	glm::vec3 c = point1 + ((point2 - point1)/2.0f);
//	glm::vec3 radius1 = glm::vec3(n1->boundingSphere.radius);	
//	glm::vec3 radius2 = glm::vec3(n2->boundingSphere.radius);

	glm::vec3 r1 = c1 - obj1->GetCentreOfMass();	
	glm::vec3 r2 = c2 - obj2->GetCentreOfMass();

	glm::vec3 contactNormal = glm::normalize(c2 - c1);
	glm::vec3 collisionPoint = c1 + ((c2 - c1)/2.0f);

	glm::vec3 p1 = obj1->GetVelocity() + glm::cross(obj1->GetAngularVelocity(), c - obj1->GetCentreOfMass());
	glm::vec3 p2 = obj2->GetVelocity() + glm::cross(obj2->GetAngularVelocity(), c - obj2->GetCentreOfMass());

	float relativeVelocity = glm::dot(contactNormal, (p1 - p2));


	if (relativeVelocity > -COLLISION_THRESHOLD && relativeVelocity < COLLISION_THRESHOLD)	//Resting contact?
	{
		obj1->Stop();
		obj2->Stop();
		return false;
	}

	if (relativeVelocity < -COLLISION_THRESHOLD)		//Moving Away
	{
		return false;
	}

	glm::mat3 I1 =  obj1->GetOrientationMatrix() * obj1->GetInverseInertiaTensor() * glm::transpose(obj1->GetOrientationMatrix()) ;
	glm::mat3 I2 =	obj2->GetOrientationMatrix() * obj2->GetInverseInertiaTensor() * glm::transpose(obj2->GetOrientationMatrix()) ;

	float N = -1.0f * (1 + epsilon) * relativeVelocity;

	float t1t2 = obj1->inverseMass + obj2->inverseMass;
	glm::vec3 t3 = glm::cross(I1 * glm::cross(r1, contactNormal), r1);
	glm::vec3 t4 = glm::cross(I2 * glm::cross(r2, contactNormal), r2);

	float t3t4 = glm::dot(t3, contactNormal) + glm::dot(t4, contactNormal);

	float denominator = t1t2 + t3t4;

	float impulse = N/denominator;

	DebugOutput::DrawLine(c1, c1 + (contactNormal * impulse));

	obj1->ApplyForce(impulse * contactNormal, c1);
	obj2->ApplyForce(impulse * -contactNormal, c2);

	return true;
}

static void ScreenPosToWorldRay(
		int mouseX, int mouseY,             // Mouse position, in pixels, from bottom-left corner of the window
		int screenWidth, int screenHeight,  // Window size, in pixels
		glm::mat4 ViewMatrix,               // Camera position and orientation
		glm::mat4 ProjectionMatrix,         // Camera parameters (ratio, field of view, near and far planes)
		glm::vec3& out_origin,              // Ouput : Origin of the ray. /!\ Starts at the near plane, so if you want the ray to start at the camera's position instead, ignore this.
		glm::vec3& out_direction            // Ouput : Direction, in world space, of the ray that goes "through" the mouse.
	)
{

	// The ray Start and End positions, in Normalized Device Coordinates
	glm::vec4 lRayStart_NDC(
		((float)mouseX/(float)screenWidth  - 0.5f) * 2.0f, // [0,1024] -> [-1,1]
		((float)mouseY/(float)screenHeight - 0.5f) * 2.0f, // [0, 768] -> [-1,1]
		-1.0, // The near plane maps to Z=-1 in Normalized Device Coordinates
		1.0f
	);
	glm::vec4 lRayEnd_NDC(
		((float)mouseX/(float)screenWidth  - 0.5f) * 2.0f,
		((float)mouseY/(float)screenHeight - 0.5f) * 2.0f,
		0.0,
		1.0f
	);


	// The Projection matrix goes from Camera Space to NDC.
	// So inverse(ProjectionMatrix) goes from NDC to Camera Space.
	glm::mat4 InverseProjectionMatrix = glm::inverse(ProjectionMatrix);
	
	// The View Matrix goes from World Space to Camera Space.
	// So inverse(ViewMatrix) goes from Camera Space to World Space.
	glm::mat4 InverseViewMatrix = glm::inverse(ViewMatrix);
	
	glm::vec4 lRayStart_camera = InverseProjectionMatrix * lRayStart_NDC;    lRayStart_camera/=lRayStart_camera.w;
	glm::vec4 lRayStart_world  = InverseViewMatrix       * lRayStart_camera; lRayStart_world /=lRayStart_world .w;
	glm::vec4 lRayEnd_camera   = InverseProjectionMatrix * lRayEnd_NDC;      lRayEnd_camera  /=lRayEnd_camera  .w;
	glm::vec4 lRayEnd_world    = InverseViewMatrix       * lRayEnd_camera;   lRayEnd_world   /=lRayEnd_world   .w;


	// Faster way (just one inverse)
	//glm::mat4 M = glm::inverse(ProjectionMatrix * ViewMatrix);
	//glm::vec4 lRayStart_world = M * lRayStart_NDC; lRayStart_world/=lRayStart_world.w;
	//glm::vec4 lRayEnd_world   = M * lRayEnd_NDC  ; lRayEnd_world  /=lRayEnd_world.w;


	glm::vec3 lRayDir_world(lRayEnd_world - lRayStart_world);
	lRayDir_world = glm::normalize(lRayDir_world);


	out_origin = glm::vec3(lRayStart_world);
	out_direction = glm::normalize(lRayDir_world);
}

static bool TestRayOBBIntersection(
		glm::vec3 ray_origin,        // Ray origin, in world space
		glm::vec3 ray_direction,     // Ray direction (NOT target position!), in world space. Must be normalize()'d.
		glm::vec3 aabb_min,          // Minimum X,Y,Z coords of the mesh when not transformed at all.
		glm::vec3 aabb_max,          // Maximum X,Y,Z coords. Often aabb_min*-1 if your mesh is centered, but it's not always the case.
		glm::mat4 ModelMatrix,       // Transformation applied to the mesh (which will thus be also applied to its bounding box)
		float& intersection_distance // Output : distance between ray_origin and the intersection with the OBB
	)
{
	// Intersection method from Real-Time Rendering and Essential Mathematics for Games
	
	float tMin = 0.0f;
	float tMax = 100000.0f;

	glm::vec3 OBBposition_worldspace(ModelMatrix[3].x, ModelMatrix[3].y, ModelMatrix[3].z);

	glm::vec3 delta = OBBposition_worldspace - ray_origin;

	// Test intersection with the 2 planes perpendicular to the OBB's X axis
	{
		glm::vec3 xaxis(ModelMatrix[0].x, ModelMatrix[0].y, ModelMatrix[0].z);
		xaxis = glm::normalize(xaxis);

		float e = glm::dot(xaxis, delta);
		float f = glm::dot(ray_direction, xaxis);
			
		if ( fabs(f) > 0.001f ){ // Standard case

			float t1 = (e+aabb_min.x)/f; // Intersection with the "left" plane
			float t2 = (e+aabb_max.x)/f; // Intersection with the "right" plane
			// t1 and t2 now contain distances betwen ray origin and ray-plane intersections

			// We want t1 to represent the nearest intersection, 
			// so if it's not the case, invert t1 and t2
			if (t1>t2){
				float w=t1;t1=t2;t2=w; // swap t1 and t2
			}

			// tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
			if ( t2 < tMax )
				tMax = t2;
			// tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
			if ( t1 > tMin )
				tMin = t1;

			// And here's the trick :
			// If "far" is closer than "near", then there is NO intersection.
			// See the images in the tutorials for the visual explanation.
			if (tMax < tMin )
				return false;

		}else{ // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
			if(-e+aabb_min.x > 0.0f || -e+aabb_max.x < 0.0f)
				return false;
		}
	}


	// Test intersection with the 2 planes perpendicular to the OBB's Y axis
	// Exactly the same thing than above.
	{
		glm::vec3 yaxis(ModelMatrix[1].x, ModelMatrix[1].y, ModelMatrix[1].z);
		yaxis = glm::normalize(yaxis);

		float e = glm::dot(yaxis, delta);
		float f = glm::dot(ray_direction, yaxis);

		if ( fabs(f) > 0.001f ){

			float t1 = (e+aabb_min.y)/f;
			float t2 = (e+aabb_max.y)/f;

			if (t1>t2){float w=t1;t1=t2;t2=w;}

			if ( t2 < tMax )
				tMax = t2;
			if ( t1 > tMin )
				tMin = t1;
			if (tMin > tMax)
				return false;

		}else{
			if(-e+aabb_min.y > 0.0f || -e+aabb_max.y < 0.0f)
				return false;
		}
	}


	// Test intersection with the 2 planes perpendicular to the OBB's Z axis
	// Exactly the same thing than above.
	{
		glm::vec3 zaxis(ModelMatrix[2].x, ModelMatrix[2].y, ModelMatrix[2].z);
		zaxis = glm::normalize(zaxis);

		float e = glm::dot(zaxis, delta);
		float f = glm::dot(ray_direction, zaxis);

		if ( fabs(f) > 0.001f ){

			float t1 = (e+aabb_min.z)/f;
			float t2 = (e+aabb_max.z)/f;

			if (t1>t2){float w=t1;t1=t2;t2=w;}

			if ( t2 < tMax )
				tMax = t2;
			if ( t1 > tMin )
				tMin = t1;
			if (tMin > tMax)
				return false;

		}else{
			if(-e+aabb_min.z > 0.0f || -e+aabb_max.z < 0.0f)
				return false;
		}
	}

	intersection_distance = tMin;
	return true;

}