#include "stdafx.h"

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <glfw3.h>

//DevIL Image Loader
#include <IL/il.h>

//GL Maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//Local Classes
#include "InstancedModel.h"
#include "model.h"
#include "ShaderManager.hpp"
#include "Camera.h"
#include "ParticleSystem.h"
#include "PhysObject.h"
#include "TextureUtils.h"
#include "SpatialGrid.h"
#include "CubeMap.hpp"
#include "Physics.h"
#include "IntersectionTest.h"
#include "DebugOutput.h"

#include "Octree.h"

#include "ParticleDemo.h"
#include "BVHDemo.h"
#include "MotionDemo.h"
#include "BroadPhaseDemo.h"
#include "NarrowPhaseDemo.h"
#include "ResponseDemo.h"
#include "FEMDemo.h"

//Target Framerate (will not exceed)
const int TARGET_FPS = 60;
//Maximum physics framerate. 
//Output can appear jumpy is this is too far above actual framerate
const int MAX_PHYS_RATE = 120;

//Handles Program Initialisation 
bool glInit();

//Data for choosing demos
enum demo {PARTICLES, RIGIDBODY_UNCONSTRAINED, BROAD_PHASE, BVH, NARROW_PHASE, COLLISION_RESPONSE, FEM, num_values};
int chosenDemo = -1;
void ChooseDemo();

//state variables for debugging
bool paused		 = false;
bool debug		 = true;
bool drawGrid	 = false;
bool drawBVH	 = false;
int	 treeLevel	 = 0;

//Camera and Input Variables
Camera camera(glm::vec3(0.0f, 0.0f, 2700.0f));

bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

// Function prototypes
//GLFW Input Callbacks
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

void handleInput();
void drawScene(glm::mat4 v, glm::mat4 p);

//Window For OpenGL App
GLFWwindow* window;
// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;
//Clipping Planes
float near = 0.1f;
float far = 10000.0f;

//For time-variant operations
GLfloat deltaTime = 0.0f;
GLfloat renderingDelta = 0.0f;
GLfloat lastFrame = 0.0f;
GLfloat lastRenderedFrame = 0.0f;	
GLfloat t = 0.0;
GLint nbFrames = 0;

DemoBase *demo;

//Boolean variables to allow rendering and physics to have different framerates
float doRendering = false;
float doPhysics = true;

GLuint particleTexture;

CubeMap cubeMap;
SpatialGrid gridSpace;

float collisionTests = 0.0f;
float frames = 0.0f;

void emitParticles(Particle* p) {
	static int i = 0;

	i = (i+1) % 360;
}

int _tmain(int argc, _TCHAR* argv[])
{
	ChooseDemo();

	//Initialise OpenGL
	if (!glInit()) {
		std::cout << "Failed to initialise OpenGL";
		std::cin;
		return -1;
	}

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Set frame time
		GLfloat currentFrame = (GLfloat) glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		if (deltaTime >= (1.0f/MAX_PHYS_RATE))
		{
			doPhysics = true;
			lastFrame = currentFrame;
		}
		else
			doPhysics = false;

		//Should lock framerate at target framerate
		renderingDelta = currentFrame - lastRenderedFrame;
		if(renderingDelta >= (1.0f/TARGET_FPS))
			doRendering = true;
		else
			doRendering = false;
		t += deltaTime;		

		if (!paused && doPhysics)
		{
			for (int i =0; i < Physics::PhysicsObjects.size(); i++)
				Physics::PhysicsObjects[i].Update(deltaTime);	

			demo->Update(deltaTime);
		}

			
		if (!doRendering)
			continue;
		lastRenderedFrame = currentFrame;

		glfwPollEvents();

		handleInput();
		demo->HandleInput(keys);

		glClear(GL_DEPTH_BUFFER_BIT); //Shouldn't need to clear color if I'm redrawing everything

		// Create camera transformation
		glm::mat4 view;
		view = camera.GetViewMatrix();
		//Create Projection Matrix
		glm::mat4 projection;	
		projection = glm::perspective(camera.Zoom, (float)WIDTH/(float)HEIGHT, near, far);

		//PhysObjects
		drawScene(view, projection);

		gridSpace.Reset();

		DebugOutput::Update(view, projection);

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}

void drawScene(glm::mat4 v, glm::mat4 p) {

	//Draw CubeMap
	cubeMap.DrawSkyBox(v, p);

	demo->Draw(v, p);

	if (debug && drawGrid)
	{
		gridSpace.Draw(v, p);
	}		
}

bool glInit() {
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
   
	// Init GLFW
	if (!glfwInit())
	{
		std::cout << "ERROR: Could not initialise GLFW...";
		return false;
	}

	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create a GLFWwindow object that we can use for GLFW's functions
	window = glfwCreateWindow(WIDTH, HEIGHT, "CS7057-Real Time Physics", nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		std::cout << "ERROR: Could not create window...";
		return false;
	}

	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);


	//Grab and hide mouse cursor
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//glfwSetCursor(window, cursor);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return false;
	}

	//Initialise DevIL image loader
	ilInit();

	//Initialise DebugOutput variables
	DebugOutput::Init();

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 
	glEnable (GL_BLEND);
//	glEnable(GL_CULL_FACE);
//	glCullFace(GL_BACK);
	glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glPointSize(5.0f);

	switch(chosenDemo)
	{
	case demo::PARTICLES:
		demo = new ParticleDemo;
		break;
	case demo::BVH:
		demo = new BVHDemo;
		break;
	case demo::RIGIDBODY_UNCONSTRAINED:
		demo = new MotionDemo;
		break;
	case demo::BROAD_PHASE:
		demo = new BroadPhaseDemo;
		break;
	case demo::NARROW_PHASE:
		demo = new NarrowPhaseDemo;
		break;
	case demo::COLLISION_RESPONSE:
		demo = new ResponseDemo;
		break;
	case demo::FEM:
		demo = new FEMDemo;
		break;
	}
	
	demo->Init();
	
	//cubemap initialisation
	cubeMap.loadCubeMap("../textures/cubemaps/space/");
	cubeMap.setShader(ShaderManager::loadShader("skybox")); //TODO - This could definitely be hard-coded...

	gridSpace = SpatialGrid(-1600.0f, 1600.0f, 500.0f);

	return true;
}

// Moves/alters the camera positions based on user input
void handleInput()
{
	if(keys[GLFW_KEY_LEFT_SHIFT])
	{
		if(keys[GLFW_KEY_B])
		{
			drawBVH = !drawBVH;
			keys[GLFW_KEY_B] = false;
		}
		if(keys[GLFW_KEY_D])
		{
			debug = !debug;
			keys[GLFW_KEY_D] = false;
		}
		if(keys[GLFW_KEY_G])
		{
			drawGrid = !drawGrid;
			keys[GLFW_KEY_G] = false;
		}
	}
	// Camera controls
	if(keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, renderingDelta);
	if(keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, renderingDelta);
	if(keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, renderingDelta);
	if(keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, renderingDelta);
	if(keys[GLFW_KEY_E])
		camera.ProcessKeyboard(UP, renderingDelta);
	if(keys[GLFW_KEY_Q])
		camera.ProcessKeyboard(DOWN, renderingDelta);

	if(keys[GLFW_KEY_P])
	{
		paused = !paused;
		keys[GLFW_KEY_P] = false;
	}

	if(keys[GLFW_KEY_SPACE])
	{
		keys[GLFW_KEY_SPACE] = false;
	}
}

//Is called when a mouse button is pushed
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
		return;
	}
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key < 0)
		return;

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if(action == GLFW_PRESS)
		keys[key] = true;
	else if(action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if(firstMouse)
	{
		lastX = (GLfloat) xpos;
		lastY = (GLfloat) ypos;
		firstMouse = false;
	}

	GLfloat xoffset = (GLfloat) xpos - lastX;
	GLfloat yoffset = lastY - (GLfloat) ypos;  // Reversed since y-coordinates go from bottom to left
	
	lastX = (GLfloat) xpos;
	lastY = (GLfloat) ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}	


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll((GLfloat)yoffset * 0.1f);
}

//TODO - Make this less ugly
void ChooseDemo()
{
	cout << "Please choose a demo \n\
1. Particles \n\
2. Unconstrained Rigid Body Motion \n\
3. Broad-phase collision detection \n\
4. Bounding Volume Hierarchy \n\
5. Narrow-phase Collision Detection \n\
6. Collision Response \n\
7. Co-Rotational Finite Element Method\n";

	std::cin >> chosenDemo;
	chosenDemo--;
	while (!(chosenDemo >= 0 && chosenDemo < demo::num_values))
	{
		std::cout << "Please enter a number between 1 and " << demo::num_values;
		std::cin >> chosenDemo;
		chosenDemo--;
	}

	switch (chosenDemo)
	{
	case(demo::PARTICLES) :
		cout << "\n In this demo a system of particles is drawn and the following forces act upon it:\n\
For the first 20s, a whirlwind style force is applied.\n\
For the next 20s, all of the particles are attracted to a point near the origin.\n\
For the next 20s, the whirlwind force is applied but with gravity too.\n\
Finally, only gravity is applied and with no ground plane, all of the particles fall away.\n\
\n\
Particles will collide with the box to the right. This is implemented with a series of plane intersection tests against its OBB.";
		break;
	case(demo::RIGIDBODY_UNCONSTRAINED) :
		cout << "\n In this demo a number of cubes are attracted to a point near the origin and their linear and angular momentums update as appropriate\n\
NOTE: The force is applied to a point above the center of mass so as to cause the cubes to spin.\n";
		break;
	case(demo::BROAD_PHASE) :
		cout << "\n In this demo a number of cubes are attracted to a point near the origin with boudning spheres drawn, their spheres will turn red when a collision occurs.\n\
NOTE: The force is applied to a point above the center of mass so as to cause the cubes to spin.\n";
		break;
	case(demo::BVH) :
		cout << "\n In this demo, a plane model is displayed and the UP/DOWN keys may be used to increase/decrease granularity of the BVH. The BVH has 6 layers including the root bounding sphere but\
for the actual collision response demo, this was reduced to 4 to save some computation.\n";
		break;
	case(demo::NARROW_PHASE) :
		cout << "\n In this demo, the cube in the middle can be steered with the arrow keys (LSHIFT + UP/DOWN to move in Z), in the case of a collision, the spheres in contact will be drawn in red.\n\
NOTE: Due to a poorly optimised narrow phase, colliding spheres may be returned multiple times and so this demo is likely to cause severe framerate drops at times.\n";
		break;
	case(demo::COLLISION_RESPONSE) :
		cout << "\n In this demo, a plane and 5 cubes are attracted to a point near the center of the screen and will collide with each other and respond appropriately.\n";
		break;
	case (demo::FEM) :
		std::cout << "This simple demo shows a beam fixed at one end deforming under its own weight using the finite element method with stiffness warping." << std::endl << std::endl
		<< "NOTE: I did not implement plastic deformation so if you turn gravity back off, the mesh will recover completely given enough time." << std::endl << std::endl
		<< "Controls: " << std::endl << std::endl
		<< "Shift + W - Wireframe Mode on/off" << std::endl
		<< "G - Gravity on/off" << std::endl
		//<< "W - Stiffness Warping on/off" << std::endl;	//Didn't make it to release
		<< "Mouse To Look Around" << std::endl << std::endl;
	default:
		break;
	}
	cout << "\nUse WASD and mouse to move camera \n";

	cout << "\nPress ESC to exit the demo. \n";


	std::cout << "\nPress enter to continue \n";
	std::cin.sync();
	std::cin.get();
}