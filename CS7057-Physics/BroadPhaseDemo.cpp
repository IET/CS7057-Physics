#include "BroadPhaseDemo.h"
#include "Physics.h"
#include "ParticleForces.h"

BroadPhaseDemo::BroadPhaseDemo(void)
{
}

void BroadPhaseDemo::Init()
{
	texShader = ShaderManager::loadShader("texture");
	rectangle = Model("../models/rectangle.obj");
	rectangle.setShader(texShader);

	PhysObject box = PhysObject(rectangle, glm::vec3(0, 0.0f, 0.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(-700.0f, 700.0f, 100.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(-600.0f, -200.0f, 300.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(0.0f, -400.0f, 100.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(500.0f, 0.0f, 300.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(-600.0f, 100.0f, 500.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);
}


void BroadPhaseDemo::Update(float dt)
{
	//Test collisions
	for (int i =0; i < Physics::PhysicsObjects.size() - 1; i++)
	{
		for (int j = i + 1; j < Physics::PhysicsObjects.size(); j++)
		{
			//collisionTests += 1.0f;
			if (IntersectionTest::TestIntersection(&Physics::PhysicsObjects[i].bs, &Physics::PhysicsObjects[j].bs))
			{
				Physics::PhysicsObjects[i].bs.isColliding = true;
				Physics::PhysicsObjects[j].bs.isColliding = true;
				continue;
			}
		}
	}

	//Apply attrction force to physics objects
	for (int i = 0; i < Physics::PhysicsObjects.size(); i++)
		Physics::PhysicsObjects[i].ApplyForce(&ParticleForces::Attraction, glm::vec3(0.0f, 20.0f, 0.0f));
}

void BroadPhaseDemo::Draw(glm::mat4 v, glm::mat4 p)
{
	for (int i =0; i < Physics::PhysicsObjects.size(); i++)
	{
		Physics::PhysicsObjects[i].Draw(v, p);

		if (Physics::PhysicsObjects[i].bs.isColliding)
			Physics::PhysicsObjects[i].bs.Draw(v, p, glm::vec3(1.0f, 0.0f, 0.0f));
		else
			Physics::PhysicsObjects[i].bs.Draw(v, p, glm::vec3(0.0f, 1.0f, 0.0f));

		Physics::PhysicsObjects[i].bs.isColliding = false;
	}
}

void BroadPhaseDemo::Cleanup()
{
}

BroadPhaseDemo::~BroadPhaseDemo(void)
{
}
