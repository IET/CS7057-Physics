#pragma once

#include "InstancedModel.h"
#include <string>
#include "PhysObject.h"

struct Particle {
	float age;
	float mass;
	glm::vec3 position;
	glm::vec3 velocity;

	glm::vec3 netForce;
};

class ParticleSystem {
public:

	ParticleSystem();
	ParticleSystem(std::string const & path, int n);
	ParticleSystem(std::string const & path, int n, void (*emissionFunc)(Particle* p), float emissionRate, glm::vec3 emissionPoint = glm::vec3(0));
	ParticleSystem( const ParticleSystem& other );
	ParticleSystem& operator=( const ParticleSystem& other );
		  
	ParticleSystem(InstancedModel *m);
	void EulerStep(float deltaTime);

	~ParticleSystem();

	void ApplyForce(glm::vec3 (*f) (Particle *p));
	void ApplyForce(glm::vec3 (*f) (glm::vec3));
	void ApplyForce(glm::vec3 f);
	
	void setNumParticles(int n);

	bool TestCollision(Particle *p, PhysObject *obj);

	void CollisionResponse(Particle *p);

	void AddCollider(PhysObject *obj);
private:
	float	particleLifeTime;
	int		numParticles;
	
	int		lastParticleEmitted;

	void	(*emissionFunction) (Particle* p);

	void	InitialiseParticlePoint(glm::vec3 p);
	void	InitialiseParticleField(float size);
	
	//Smarter Particle Emission
	glm::vec3	emissionPoint;
	float		emissionRate;
	void		Emit(float deltaTime);

	InstancedModel*	model;
	Particle*		particles;

	//TODO - Less cache trashing
	std::vector<PhysObject *> potentialColliders;
};