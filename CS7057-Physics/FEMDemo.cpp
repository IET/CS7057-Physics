#include "FEMDemo.h"
#include "glfw3.h"


FEMDemo::FEMDemo()
{
}

void FEMDemo::Init()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	//Load Shaders
	colShader = ShaderManager::loadShader("color");

	tetMesh.LoadMesh("../models/Tetrahedral/Box/Box.1.mesh");
	tetMesh.shader = colShader;
	tetMesh.SetFixedFace(TetrahedralMesh::Face::LEFT);

	doGravity = false;
}

void FEMDemo::Update(float dt)
{
	if (doGravity)
		tetMesh.ApplyConstantAcceleration(glm::vec3(0, -9.81f, 0));
	tetMesh.Update(dt);
}

void FEMDemo::Draw(glm::mat4 v, glm::mat4 p)
{
	// Don't use the normal skybox for this one
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	tetMesh.shader->enableShader();
	tetMesh.shader->setUniformVector3fv("col", glm::vec3(0.3f, 0.0f, 0.0f));
	tetMesh.Draw(v, p);
	tetMesh.shader->disableShader();
}

void FEMDemo::HandleInput(bool *keys)
{
	if (keys[GLFW_KEY_G])
	{
		doGravity = !doGravity;
		std::cout << "Gravity is: " << (doGravity ? "ON" : "OFF") << std::endl << std::endl;
		keys[GLFW_KEY_G] = false;
	}

	if (keys[GLFW_KEY_W] && (keys[GLFW_KEY_LEFT_SHIFT] || keys[GLFW_KEY_RIGHT_SHIFT]))
	{
		tetMesh.isWireframe = !tetMesh.isWireframe;
		std::cout << "Wireframe Mode is: " << (tetMesh.isWireframe ? "ON" : "OFF") << std::endl << std::endl;
		keys[GLFW_KEY_W] = false;
	}
}

void FEMDemo::Cleanup() {}

FEMDemo::~FEMDemo()
{
}
