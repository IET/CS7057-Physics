#include "MotionDemo.h"
#include "Physics.h"
#include "ParticleForces.h"

MotionDemo::MotionDemo(void)
{
}

void MotionDemo::Init()
{
	texShader = ShaderManager::loadShader("texture");
	rectangle = Model("../models/rectangle.obj");
	rectangle.setShader(texShader);

	PhysObject box = PhysObject(rectangle, glm::vec3(0, 0.0f, 0.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(-700.0f, 700.0f, 100.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(-600.0f, -200.0f, 300.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(0.0f, -400.0f, 100.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(500.0f, 0.0f, 300.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);

	box = PhysObject(rectangle, glm::vec3(-600.0f, 100.0f, 500.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	Physics::PhysicsObjects.push_back(box);
}


void MotionDemo::Update(float dt)
{
	for (int i = 0; i < Physics::PhysicsObjects.size(); i++)
		Physics::PhysicsObjects[i].ApplyForce(&(ParticleForces::Attraction), glm::vec3(0.0f, 20.0f, 0.0f));
}

void MotionDemo::Draw(glm::mat4 v, glm::mat4 p)
{
	for (int i =0; i < Physics::PhysicsObjects.size(); i++)
		Physics::PhysicsObjects[i].Draw(v, p);
}

void MotionDemo::Cleanup()
{
}

MotionDemo::~MotionDemo(void)
{
}
