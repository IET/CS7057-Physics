#pragma once

#include <IL\il.h>
#include <sstream>
#include <iostream>
//#include <Windows.h>
#include <GL\glew.h>
//#include <GL.h>
#include "Shader.hpp"
#include <glm/glm.hpp>

class CubeMap{
private:
	GLuint vao;
	GLuint vbo;

	GLuint texID;
	
	Shader* shader;

	//static ILuint* imageIDs;
	//static GLuint* textureIDs;
	void create_cube_map (
	const char* front,
	const char* back,
	const char* top,
	const char* bottom,
	const char* left,
	const char* right,
	GLuint* tex_cube);

	bool CubeMap::load_cube_map_side (GLuint texture, GLenum side_target, const char* file_name);

public:
	CubeMap();
	CubeMap(std::string name);

	Shader* getShader();
	void setShader(Shader* s);

	void loadCubeMap(const char* mapFolder);
	void DrawSkyBox(glm::mat4 view, glm::mat4 projection);
	void use();

	GLuint getTexID();

};