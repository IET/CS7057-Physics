#pragma once
//GL Maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class ParticleForces{
public:
	static glm::vec3 Whirlwind(glm::vec3 v){
	
		float strength = 100.0f;
		glm::vec3 location = glm::vec3(0.0f, 0, 100.0f);

		float dist = glm::distance(v, location);
		float radius = 3000.0;

		glm::vec3 t_force = glm::vec3(0,0,0);
		if (glm::distance(v, location) < radius)
		{
			glm::vec3 cross = glm::cross(v - location, glm::vec3(0, 0, 1));
			t_force += glm::normalize( glm::vec3(cross.x, cross.y, 0.0f)) * strength;
		
			//t_force *= glm::distance(v, location);
		}

		glm::vec3 c_force = glm::normalize(glm::vec3(-1.0f * (v.x - location.x) , -1.0f * (v.y - location.y), 0.0f));
		c_force *= strength * 2.5f;
	
		glm::vec3 u_force = glm::vec3(0,1,0) * (sqrt(dist * dist));
		u_force = glm::vec3(0);

		glm::vec3 force = t_force + c_force + u_force;
		return force ;/// (sqrt(dist * dist));
	}

	static glm::vec3 Attraction(glm::vec3 v)
	{
		glm::vec3 location = glm::vec3(0,200,0);
		float strength = 10000.0f;

		float radius = 20.0f;

		glm::vec3 direction = glm::normalize(location - v);
		float dist = glm::distance(v, location);

		glm::vec3 force =  (strength * direction)/(dist);

		if (dist <= radius)
		{
			return glm::vec3(0);
		}
		return force;
	}

	static glm::vec3 Propulsion(glm::vec3 v)
	{
		glm::vec3 location = glm::vec3(0,0,0);
		float strength = 10.0f;

		float radius = 1000.0f;

		glm::vec3 direction = glm::normalize(location - v);
		float dist = glm::distance(v, location);

		if (dist <= radius)
		{
			return glm::vec3(strength, strength/10.0f, 0.0f);
		}

		return glm::vec3(0.0f, 0.0f, 0.0f);
	}
};