#pragma once
#include "demobase.h"

class BVHDemo :
	public DemoBase
{
private:
	static const int MAX_DEPTH = 6;

	Model planeModel;
	PhysObject plane;

	Shader *texShader;
	int treeLevel;

public:
	BVHDemo(void);

	void Init();

	void Update(float dt);

	void Draw(glm::mat4 v, glm::mat4 p);
	
	void Cleanup();
	
	void HandleInput(bool *keys);

	~BVHDemo(void);
};

