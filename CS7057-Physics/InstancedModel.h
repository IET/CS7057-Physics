#pragma once

#include "model.h"
#include "InstancedMesh.h"
#include "Shader.hpp"


class InstancedModel : public Model {
public:

	InstancedModel();
	InstancedModel(int n);

	//TODO - This shouldn't really be public
	glm::vec3* positions;

	// Constructor, expects a filepath to a 3D model. Allows instanced rendering.
	InstancedModel(string const & path, int instances, bool gamma = false);
	//Copy Constructor and assignment operator
	InstancedModel(const InstancedModel &other);
	InstancedModel &InstancedModel::operator=(const InstancedModel &other);

	//Hiding parent methods
	//Draw the model using instanced rendering
	void Draw();
	void Draw(glm::mat4 &v, glm::mat4 &p);

	//Getters and Setters
	int getInstances();

	InstancedModel::~InstancedModel();
private:
	int instances;

	std::vector<InstancedMesh> meshes;
	
	//TODO - find a better method than just hiding all of these
	//Hiding parent methods
	InstancedMesh InstancedModel::processMesh(aiMesh* mesh, const aiScene* scene);
	void processNode(aiNode* node, const aiScene* scene);
	void loadModel(std::string path);

	void setInstances(int n);
};