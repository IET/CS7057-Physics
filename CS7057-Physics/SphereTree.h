#pragma once
#include <vector>
#include "model.h"
#include "BoundingSphere.h"

struct BVHNode {
	std::vector<Triangle*> triangles;
	std::vector<BVHNode*> children;
	BoundingSphere boundingSphere;

	BVHNode *neighbour;

	BVHNode()
	{
		neighbour = nullptr;
	}

	BVHNode (std::vector<Triangle*> tris)
	{
		this->triangles = tris;
		boundingSphere = BoundingSphere(tris);
		neighbour = nullptr;
	}

	~BVHNode()
	{
		for (int i = 0; i < children.size(); i++)
		{
			delete children[i];
		}
	}
};

class SphereTree
{
public:
	SphereTree(void);
	void Draw(glm::mat4 &v, glm::mat4 &p, int level);
	BVHNode * rootNode;

	~SphereTree(void);

protected:
	int depth;
	void DrawNodes(BVHNode* node, glm::mat4 &v, glm::mat4 &p, int level);
	virtual void GenerateSpheres(BVHNode*& node, int depth) = 0;
	
	void ConnectNeighbours();
	void FindNeighbours(BVHNode *node, std::vector<BVHNode*> &connections, int level);

};

