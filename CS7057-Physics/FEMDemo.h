#pragma once
#include "DemoBase.h"
#include "TetrahedralMesh.h"

class FEMDemo :
	public DemoBase
{
private:
	Shader *colShader;
	TetrahedralMesh tetMesh;
	bool doGravity;
public:
	FEMDemo();

	void Init();

	void Draw(glm::mat4 v, glm::mat4 p);

	void Update(float dt);

	void HandleInput(bool *keys);

	void Cleanup();

	~FEMDemo();
};

