#include "InstancedMesh.h"

InstancedMesh::InstancedMesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> texture, int instances, glm::vec3 *positions)
{
	this->instances		= instances;
	this->positions		= positions;

	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	// Now that we have all the required data, set the vertex buffers and its attribute pointers.
	this->setupMesh();
}

InstancedMesh::InstancedMesh(const InstancedMesh &other)
	:Mesh(other)
{
	instances = other.instances;
	vertices = other.vertices;
	indices = other.indices;
	textures = other.textures;
	positions = other.positions;
}


void InstancedMesh::Draw() {
/* initialize random seed: */
	srand (time(NULL));

	float randRed = ((float) (rand() % 100 + 50))/100.0f;		
	float randGreen = ((float) (rand() % 100 + 50))/100.0f;
	float randBlue = ((float) (rand() % 100 + 50))/100.0f;

	// Draw mesh

	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

	glBufferSubData(GL_ARRAY_BUFFER, 0,instances * sizeof(glm::vec3), &positions[0]);

	glDrawArrays(GL_POINTS, 0, instances);
	glBindVertexArray(0);
}

void InstancedMesh::Draw(glm::mat4 &v, glm::mat4 &p)
{
	// Draw mesh
	glBindVertexArray(this->VAO);

	glBindBuffer(GL_ARRAY_BUFFER, this->positionBuffer);

	glBufferSubData(GL_ARRAY_BUFFER, 0,instances * sizeof(glm::vec3), &positions[0]);

	glDrawElementsInstanced(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0, instances);
	glBindVertexArray(0);
}

void InstancedMesh::setupMesh() {
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	// Load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	// A great thing about structs is that their memory layout is sequential for all its items.
	// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
	// again translates to 3/2 floats which translates to a byte array.
//  glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);  
	glBufferData(GL_ARRAY_BUFFER, instances * sizeof(glm::vec3), &positions[0], GL_STREAM_DRAW);

 //   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

	// Set the vertex attribute pointers
	// Vertex Positions
	glEnableVertexAttribArray(0);	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,sizeof(glm::vec3), (GLvoid*)0);

	// Vertex Texture Coords
 //   glEnableVertexAttribArray(2);	
 //   glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));	//6
/*
	glGenBuffers(1, &this->positionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, instances * sizeof(glm::vec3), &positions[0], GL_STREAM_DRAW);
	
	// Set attribute pointers for position (3 * float)
	glEnableVertexAttribArray(1); 
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (GLvoid*)0);
			
	glVertexAttribDivisor(0, 0); // particles vertices : always reuse the same 4 vertices -> 0
	glVertexAttribDivisor(1, 1); // positions
	//glVertexAttribDivisor(2, 1); // color
*/
	glBindVertexArray(0);
		
}

InstancedMesh::~InstancedMesh(){
	//glDeleteBuffers(1, &this->VBO);
	//glDeleteBuffers(1, &this->EBO);
	//glDeleteBuffers(4, &this->matrixBuffer);
	//delete[] positions;
}