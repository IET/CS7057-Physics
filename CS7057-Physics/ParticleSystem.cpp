#include "ParticleSystem.h"

ParticleSystem::ParticleSystem()
{
	this-> particles = new Particle[1000];
	this->emissionFunction = nullptr;
}


ParticleSystem::ParticleSystem(InstancedModel *m) 
{
	this->numParticles = m->getInstances();
	this->model = m;

	this-> particles = new Particle[this->numParticles];

	particleLifeTime = 1000.0f;
	InitialiseParticleField(5000.0f);

	this->emissionFunction = nullptr;
}

ParticleSystem::ParticleSystem(std::string const & path, int n){
	this->model = new InstancedModel(path, n);
	this->numParticles = n;

	this-> particles = new Particle[this->numParticles];

	particleLifeTime = 1000.0f;
	InitialiseParticleField(100000.0f);

	this->emissionFunction = nullptr;
}


ParticleSystem::ParticleSystem(std::string const & path, int n, void (*emissionFunc)(Particle* p), float emissionRate, glm::vec3 emissionPoint)
{
	this->model = new InstancedModel(path, n);
	this->numParticles = model->getInstances();

	this-> particles = new Particle[this->numParticles];
	//InitialiseParticleField(1000.0f);

	this->particleLifeTime = 100.0f;

	this->emissionFunction = emissionFunc;
	this->emissionRate = emissionRate;
	this->emissionPoint = emissionPoint;
}

//Copy Constructor
ParticleSystem::ParticleSystem( const ParticleSystem& other )
{
	particleLifeTime = other.particleLifeTime;
	numParticles = other.numParticles;
	lastParticleEmitted = other.lastParticleEmitted;
	
	emissionPoint = other.emissionPoint;
	emissionRate = other.emissionRate;
	emissionFunction = other.emissionFunction;

	model = other.model;
	particles = new Particle[numParticles];
	particles = other.particles;

	for (int i = 0; i < other.potentialColliders.size(); ++i)
		potentialColliders.push_back(other.potentialColliders[i]);
}

ParticleSystem& ParticleSystem::operator=( const ParticleSystem& other ) {
	particleLifeTime = other.particleLifeTime;
	numParticles = other.numParticles;
	lastParticleEmitted = other.lastParticleEmitted;
	
	emissionPoint = other.emissionPoint;
	emissionRate = other.emissionRate;
	emissionFunction = other.emissionFunction;

//	model = new InstancedModel();
	model = other.model;

	particles = new Particle[numParticles];
	for (int i = 0; i < numParticles; ++i)
		particles[i] = other.particles[i];

	for (int i = 0; i < other.potentialColliders.size(); ++i)
		potentialColliders.push_back(other.potentialColliders[i]);
	
	return *this;
}

void ParticleSystem::setNumParticles(int n){
	numParticles = n;
}

void ParticleSystem::AddCollider(PhysObject *obj)
{
	potentialColliders.push_back(obj);
}

void ParticleSystem::InitialiseParticlePoint(glm::vec3 p)
{
	for (int i =0; i < numParticles; i++){

		particles[i].position = glm::vec3(0);
		particles[i].velocity = glm::vec3(0,0,0); 
		particles[i].netForce = glm::vec3(0,0,0); 

		particles[i].mass = 1.0f;
		particles[i].age = ((float) (rand() % (int)(100)) /10.0f) + 90.0f;
	}
}

void ParticleSystem::InitialiseParticleField(float size){
	float xDisp, yDisp, zDisp;

	srand(time(NULL) /time(NULL));

	for (int i =0; i < numParticles; i++){

		Particle *p = &particles[i];
		
		//Spread particles in a 1000x1000 square
		xDisp = yDisp = zDisp = - (size / 2);
		
		xDisp += ((float) (rand() % (int)(size * 10)) ) / 10.0f;
		yDisp += ((float) (rand() % (int)(size * 10)) ) / 10.0f;
		zDisp += ((float) (rand() % (int)(size)) ) / 10.0f;

		particles[i].position = glm::vec3(xDisp, yDisp, zDisp); 
		particles[i].velocity = glm::vec3(0,0,0); 
		particles[i].netForce = glm::vec3(0,0,0); 

		particles[i].mass = 1.0f;
		particles[i].age = (rand() % ((int)particleLifeTime * 100)) / 100.0f;
	}
	
}

void ParticleSystem::EulerStep(float deltaTime)
{
	for (int i =0; i < numParticles; i++){
		Particle* p = &particles[i];
		
		
		for (int i =0; i < potentialColliders.size(); i++)
		{
			TestCollision(p, potentialColliders[i]);	
		}

		(p->velocity) = ((p->netForce) / p->mass) * deltaTime;
		(p->position) = (p->position) + ((p->velocity) * deltaTime);
		p->age += deltaTime;

		if(p->age > particleLifeTime){
			p->position = glm::vec3(-100.0f, 0.0f, 0.0f);
			p->netForce = glm::vec3(0.0f, 0.0f, 0.0f);
			p->velocity = glm::vec3(0.0f, 0.0f, 0.0f);
			p->age = 0;
		}

		if (this->emissionFunction != nullptr)
			Emit(deltaTime);

		//TODO - This step shouldn't really be necessary. Some serious data duplication here.
		model->positions[i] = particles[i].position;
	}
}

bool ParticleSystem::TestCollision(Particle *p, PhysObject *obj)
{
	float threshold = 0.1f;

	glm::vec3 contactPoint;
	glm::vec3 n;

	for (int i = 0; i < 6; i++)
	{
		int index =i;
		index %= 3;
		index = 4 >> index;

		//This is just me being too lazy to write a case for each of the 6 normals
		n = glm::vec3((index & 0x04) >> 2, (index & 0x02) >> 1, (index & 0x01)); 

		if (i > 2)
		{
			contactPoint = glm::vec3(obj->bv.GetWorldMinBounds()) * n;
		}
		else
		{
			contactPoint = glm::vec3(obj->bv.GetWorldMaxBounds()) * n;
		}

		n = glm::normalize(contactPoint - ((obj->GetPosition()*n)) );

		if ( !( glm::dot(p->position - contactPoint, n) < threshold ) )
		{
			return false;
		}


	}

	for (int i = 0; i < 6; i++)
	{
		int index =i;
		index %= 3;
		index = 4 >> index;

		n = glm::vec3((index & 0x04) >> 2, (index & 0x02) >> 1, (index & 0x01)); 

		if (i > 2)
		{
			contactPoint = glm::vec3(obj->bv.GetWorldMinBounds()) * n;
		}
		else
		{
			contactPoint = glm::vec3(obj->bv.GetWorldMaxBounds()) * n;
		}

		n = glm::normalize(contactPoint - ((obj->GetPosition()*n)) );
		if (glm::dot(n, (p->velocity)) < threshold )
		{
			p->netForce *= (-1.0f * obj->GetRestitutionCoeffcient() * n);
			break;
		}
	}

	//Apply some random noise to approximate surface roughness
	p->netForce += glm::vec3(((float)(rand() %100)), ((float)(rand() %100)), ((float)(rand() %100)));
	return true;
}

void ParticleSystem::Emit(float timeStep)
{
	for (int i = lastParticleEmitted; i < (timeStep * emissionRate); i++)
	{
		emissionFunction(&particles[i]);
		lastParticleEmitted = (lastParticleEmitted) % numParticles;
	}
}

void ParticleSystem::CollisionResponse(Particle *p)
{
	p->velocity *= -1;
}

//Apply Force function accepting a function as parameter
//Allows the modelling of force fields where force applied is a function of location
void ParticleSystem::ApplyForce(glm::vec3 (*f) (Particle *p))
{
	for (int i =0; i < numParticles; i++)
	{
		(particles[i].netForce) += (*f)(&particles[i]);
	}
}

//Apply Force function accepting a function as parameter
//Allows the modelling of force fields where force applied is a function of location
void ParticleSystem::ApplyForce(glm::vec3 (*f) (glm::vec3))
{
	for (int i =0; i < numParticles; i++)
	{
		(particles[i].netForce) += (*f)(particles[i].position);
	}
}

//Apply Force function accepting a vector parameter
//Simply applies this force to all particles in the system
void ParticleSystem::ApplyForce(glm::vec3 f) 
{
	for (int i =0; i < numParticles; i++)
	{
		particles[i].netForce += f;
	}
}

ParticleSystem::~ParticleSystem() {
	if (particles != nullptr)		
		delete[] particles;
}