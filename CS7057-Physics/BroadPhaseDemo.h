#pragma once
#include "demobase.h"

class BroadPhaseDemo :
	public DemoBase
{
private:
	Shader *texShader;
	Model rectangle;

public:
	BroadPhaseDemo(void);

	void Init();

	void Update(float dt);

	void Draw(glm::mat4 v, glm::mat4 p);
	
	void Cleanup();

	~BroadPhaseDemo(void);
};

