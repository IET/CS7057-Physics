#include "ParticleDemo.h"


ParticleDemo::ParticleDemo(void)
{
}

void ParticleDemo::Init()
{
	particles = InstancedModel("../models/sphere.obj", NUM_PARTICLES);
	particleSystem = ParticleSystem(&particles);

	texShader = ShaderManager::loadShader("texture");
	rectangle = Model("../models/rectangle.obj");
	rectangle.setShader(texShader);
		
	box = PhysObject(rectangle, glm::vec3(1500.0f, 0.0f, -2500.0f), glm::vec3(0), glm::vec3(0), glm::vec3(0));
	box.setTexture(TextureUtils::LoadTextureFromFile("../textures/bricks/diffuse.jpg") );
	box.SetScale(glm::vec3(25));
	box.Update(0.0f);

	particleSystem.AddCollider(&box);

	t = 0;
}

void ParticleDemo::Update(float dt)
{
	particleSystem.EulerStep(dt);
	box.Update(dt);
	t += dt;

	//Force fields
	if (t < 20.0f)
	{
		particleSystem.ApplyForce(&ParticleForces::Whirlwind);
	}
	else if (t < 40.0f)
	{
		particleSystem.ApplyForce(&ParticleForces::Attraction);
	}
	else if (t < 60.0f)
	{
		particleSystem.ApplyForce(glm::vec3(0.0f, -9.8f, 0.0f));
		particleSystem.ApplyForce(&ParticleForces::Whirlwind);
	}
	else // Constant gravitational force
		particleSystem.ApplyForce(glm::vec3(0.0f, -9.8f, 0.0f));
}

void ParticleDemo::Draw(glm::mat4 v, glm::mat4 p)
{
	particles.getShader()->enableShader();
	particles.getShader()->setUniformMatrix4fv("projection", p);
	particles.getShader()->setUniformMatrix4fv("view", v);
	particles.Draw();
	particles.getShader()->disableShader();

	box.Draw(v, p);
}


void ParticleDemo::Cleanup()
{
}

ParticleDemo::~ParticleDemo(void)
{
}
