#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "PhysObject.h"
#include "Shader.hpp"
#include "ShaderManager.hpp"

class SpatialGrid
{
//TODO - Implement this as a singleton
	static std::vector<std::vector<PhysObject*>> grid;

public:
	float min, max;
	float cellSize;

	SpatialGrid(void);
	SpatialGrid(float min, float max, float cell_size);

	void Insert(PhysObject *p);
	void Insert(PhysObject *p, glm::vec3 position);

	void Draw(glm::mat4 v, glm::mat4 p);

	void Reset();
	void TestCollisions();

	int HashFunction(glm::vec3 p);

	int frames;
	int collisionTests;

	~SpatialGrid(void);
private:
	Shader* shader;
	//Data for drawing
	int numVertices;
	GLuint VAO, VBO;
};

