#pragma once
//GL Maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "TextureUtils.h"
#include "model.h"
#include "physics.h"

class DemoBase
{
public:
	// Each demo must implement at least these three methods
	virtual void Init() = 0;

	virtual void Update(float dt) = 0;

	virtual void Draw(glm::mat4 v, glm::mat4 p) = 0;

	virtual void HandleInput(bool *keys) {};

	virtual void Cleanup() = 0;
};