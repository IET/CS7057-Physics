#pragma once
#include "BoundingVolume.h"
#include "ShaderManager.hpp"

class OBB : public BoundingVolume
{
public:
	OBB(void);
	OBB(glm::vec4 minBounds, glm::vec4 maxBounds);

	~OBB(void);

	bool TestCollision(OBB* obb);

	//void Update(glm::mat4 *modelMat);

	void Draw(glm::mat4 v, glm::mat4 p);
};

